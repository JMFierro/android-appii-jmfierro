## Ejercicio II - Facebook, Foto, Localización y Métricas##

(Youtube: http://youtu.be/b78ZWBV5J0I)


***Jose Manuel Fierro Conchouso***

*U-Tad., Clase Android, 2013*

# Comentarios sobre el desarrollo #

### Vistas ###

La aplicación consta de una vista en la que esta situando en primer término un mapa de google. Sobre él mapa, tapándolo parcialmente,  un fragmento en el que está colocada la dirección donde se encuentra el usuario, un  **com.facebook.widget.ProfilePictureView** de *facebook* con un botón para hacer *logi/logout*, una **brújula** con movimiento y subtexto indicando el punto cardinal y finalmente un **botón para publicar**. Esta parte consta de **4** **fragmentos**:
*mapa, dirección del usuario, facebook, y brujula*.   

Debajo del mapa hay **un** **fragmento** para el *listado de las fotos*, en el se muestran las fotos que se han ido tomando. A la derecha de este (en pantallas de 7 pulgadas o más), se sitúa la cámara. 

En dispositivos de menos de 7 pulgadas los elementos son situados verticalmente, unos debajo de otros.

Para completar el aspecto sitúan varias **líneas divisorias** entre las distintas vistas. 

*Nota: Todos los nombres de métodos y atributos los he escrito en ingles. Fernando comento en clase que la mezcla con el español quedaba "cutre". Sólo he mantenido en el castellano los comentarios.*

### Google Maps API V3 ###

En *"MapGooogleFragment.java"* se gestiona el mapa de google. El mapa es completamente operativo, se puede pulsar el botón de localización, hacer zoom, desplazar el mapa, etc. 

Al entrar en la aplicacción posiciono el mapa en la localización actual con un zoom de 15. Con el método *"setLocationMap()"*, *MainActivity.java* puede indicarle a la  *"MappGoogleFragment.java"* las coordenadas en donde posicionarse:

    	map.setLocationMap(mLocationFrag.getLatitud(), mLocationFrag.getLongitud()); 


trasa sacar una fotogrfía se coloca un "thumbal" de la imagen sobre el mapa de google, en la posición actual y como titulo le proporciono la dirección actual. A la imagen le aplico una forma redonda antes de situarla en el mapa. Utilizo el método *"setImage()* para comunicarle a *"MapGoogleFragment.java"* la imagen a mostrar:

    public void setImageMap (Bitmap imageBitmap, String titleText) {
        
        LatLng location = getLocation(getActivity());
        if (location != null)
        	setImageMap(imageBitmap, 95, titleText, location.latitude, location.longitude, 15);
    }

    public void setImageMap (Bitmap imageBitmap, String titleText, double latitudDouble, double longitudDouble) {
        
    	setImageMap(imageBitmap, 95, titleText, latitudDouble, longitudDouble, 15);
    }

    
    public void setImageMap (Bitmap imageBitmap, int thumbailSizeInt, String titleText, double latitudDouble, double longitudDouble, int zoomInt) {
        
    	if (imageBitmap != null) {
    		Bitmap photoTumbails = Bitmap.createScaledBitmap(imageBitmap, thumbailSizeInt, thumbailSizeInt, false);
    		mMap.addMarker(new MarkerOptions().position(
    				new LatLng(latitudDouble, longitudDouble)).title(titleText)
    				.icon(BitmapDescriptorFactory.fromBitmap(photoTumbails)));
    	}
    	mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitudDouble,longitudDouble)));
    }


### Facebook SDK 3.0 for Android###

Muestra un **ProfilePictureView**, que actualiza con la imagen del usuario tras hacer *login. 

         @Override
	            public void onCompleted(Response response) {
	                GraphObject graphObject = response.getGraphObject();
	                if(graphObject != null) {
	                    try {

	                    	// ProfilePicture del usuario
	                    	JSONObject jsonObject = graphObject.getInnerJSONObject();
	                        String userId = jsonObject.getString("id");
                    	    mProfilePicture.setProfileId(userId);


Para el botón *login/logout* utiliza el proporcionado por la librería de facebook: **com.facebook.widget.LoginButton**. Si el usuario hace *"login"*, se guarda en preferencias un *boolean* para recordarlo la próxima vez que se entre en la aplicación y el usuario no tenga necesidad de pulsar el botón de *login*. De la misma manera sucede con si se pulsa *logout*.

Tiene un botón para **publicar**. He construido dos con el logo de Facebook, **azul** para cuando esta activado y **gris** para cuando no. El botón se activa tras hacer una fotografía con la *cámara* o cuando se selecciona un elemento dela *listado de fotos*. Al activarse hace una **animación cuando se activa**.

Se gestiona todo desde un fragmento: **FacebookFragment.java**, con los siguientes *callbacks*:

    public interface OnFecebookListener {
    	public void onSessionOpened ();
    	public void onCompleted(String msgResponse);
    	public void onSessionException(String msgException);
    	public void onCallReintent();
    	public void onCallException(String msgException);
    	public void onButtonLoginPress();
    	public void onButtonLogoutPress();
    	public void onButtonPublishPress();
    }
    

en los que principalmente lanzo eventos para métricas en "Gogle Analytics".

Mediante los siguientes métodos, *"FacebookFragment.java"* recibe desde *"MainActivity.java"*
la *"imagen"* y el *"mensaje"* que se debe publicar.

	    public void setPhoto(Bitmap photo) {
	    	this.mPhoto = photo;
	    }
	   
	    public void setMsgPhoto(String msgPhoto) {
	    	this.mMsgPhoto = msgPhoto;
	    }
	     

### Brújula ###


En *CompassFragment.java* implmenta la visulización y movimiento de una brújula. Se utiliza el sensor de orientación:

        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);

obtiene la orientación del *dispositivo* en grados mediante le método:

    @Override
    public void onSensorChanged(SensorEvent event) {
	}

En él se pasan los grados a *"un punto cardinal"* y rota la imagen de la brújula para mantenerla apuntando al norte magnético.

        // Rotacion eje-z.
        float degree = Math.round(event.values[0]);
        String orientation = degreesToCardinalPoint((int)degree, 20);
        orientation = Integer.toString((int)degree)
        		+ "º"
        		+ " " 
        		+ orientation;

        ...

        // Animación.
        RotateAnimation ra = new RotateAnimation(
                currentDegree, 
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f, 
                Animation.RELATIVE_TO_SELF,
                0.5f);

        ra.setDuration(210);
        ra.setFillAfter(true);

        image.startAnimation(ra);

 
Un *callback* mantiene actualizado un *string* con la información relativa a la orientación (el que será utilizado para enviar junto con la fotografía al publicar en Facebook):

	@Override
	public void onSetOrientation(String orientation) {
		
		mOrientation = orientation;
	}

En *"onPause()* dejo de escuchar:

    @Override
	public void onPause() {
        super.onPause();
        
        mSensorManager.unregisterListener(this);
    }

### Listado de fotos ###

*ListPhotosFragment.java* crea una *listView* que muestra las fotos guardadas. Las fotos que se vayan haciendo con la cámara de la aplicación se van almacenando y se muestran en este listado. 

Al pulsar un *elemento* del listado se muestra la foto junto con dos animaciónes: una para la imagen a mostrar y otra para el bóton *publish* de Facebook *(si se esta en estado "login2)*. La foto mostrada puede publicarse en Facebook. Al publicarse se hace otra animación con la imagen desapareciendo en una cámara.

Para informar de que se ha seleccionado un elemento asi como de la imagen, se  utiliza dos callbacks:

	/**
	 * Callback de la clase ListPhotosFragment
	 */
	@Override
		public void onSetPhotoForPublish(Bitmap photoForPublis) {
			mPhoto = photoForPublis; 
			mMsgPhoto = mAddress
					+ "\n" + "(" + mOrientation +")";			
			FacebookFragment facebookFrang = (FacebookFragment) getSupportFragmentManager().findFragmentById(R.id.facebookFragment);
			facebookFrang.setPhoto(mPhoto);
			facebookFrang.setMsgPhoto(mMsgPhoto);
			facebookFrang.updateButtonPublish(true);
		
	}
   
	@Override
	public void onClickItem() {
        // Métrica
		mGaTracker.sendEvent(getString(R.string.list_photos), getString(R.string.push), getString(R.string.list_photos_click_item), 0L);
		
	}

Ahora al escribir el *"Readme"* me doy cuenta  de que con un sólo método hu biera sido suficiente.


### Cámara ###

Utiliza la clase Camara. En Camera.open(CAMERA_ID) utilizo 0, de esa forma si el dispositivo sólo tiene la cámara frontal*(como el Nexus 7)* será la que use:

			// Nueva camara
			final Camera camera = Camera.open(CAMERA_ID); // attempt to get a Camera instance

En el método *"startCamera()* en *"MainActivity.java"* se crea un *"preview"* 

			// Crea un preview
	        mPreview = new CameraPreview(this, camera);
	        mPreviewCameraFrameLayout.addView(mPreview);

y se escucha los botones de la cámara *(toma de foto y abortar)*: 


	        // Listener para el boton de foto.
	        ImageView captureButton = (ImageView) findViewById(R.id.takePhotoImageView);
	        captureButton.setOnClickListener(
	            new View.OnClickListener() {
	                @Override
	                public void onClick(View v) {
	            		// Métrica
	            		mGaTracker.sendEvent("takePhotoImageView", getString(R.string.push), getString(R.string.camara_take_photo), 0L);
	                	camera.takePicture(null, null, onSetCameraCallback);
	                }
	            }
	        );

	        // Listener para el boton abortar.
	        final ImageView rejectButton = (ImageView) findViewById(R.id.rejectPhotoImageView);
	        rejectButton.setOnClickListener(
	            new View.OnClickListener() {
	                @Override
	                public void onClick(View v) {
	                    if(mCamera!=null){
	                    	mCamera.stopPreview();
	                    }
	            		updateButtonsCamera(false);
	            		mPreviewCameraFrameLayout.setVisibility(View.GONE);
	            		ImageView cameraPreviewPhoto  = (ImageView) findViewById(R.id.camera_preview_photo);
	            		cameraPreviewPhoto.setVisibility(View.INVISIBLE);
	                    
	                    // Acelerometro
	                    if (mShake != null)
	                		mShake.unregisterSensor();
	            		mShake = new Shake(MainActivity.this, MAX_SHAKE_X, MAX_SHAKE_Y, MAX_SHAKE_Z);

	                    // Métrica
	            		mGaTracker.sendEvent("rejectPhotoImageView", getString(R.string.push), getString(R.string.camara_reject_photo), 0L);
	                }
	            }
	        );


En el método *surfaceChanged*() de *CameraPreview.java* gestiona el *preview**(start,stop,...)* y en lugar de utilizar *surfaceCreated() y surfaceDestroyed()*, en *onPause()* de *MainActivity.java* **libero la cámara y el preview** para que quede disponible para otras aplicaciones. No la recupero *onResum()*, el usuario tiene que llamar a la cámara si quiere volver al *preview*:


		@Override
		protected void onPause() {
		Log.d(getClass().getSimpleName(), "onPause()");

    	if (mShake != null)
    		mShake.unregisterSensor();
		
        if(mCamera!=null){
        	mCamera.stopPreview();
        	mCamera.setPreviewCallback(null);

        	mCamera.release();
        	mCamera = null;
        }
		super.onPause();
	}

Sobre el preview pongo dos botones: uno para tomar la foto y otro para rechazar.

    <FrameLayout
        android:id="@+id/camera_layout"
        android:layout_width="match_parent"
        android:layout_height="match_parent" >

            ...

        <FrameLayout
            android:id="@+id/camera_preview"
            android:layout_width="match_parent"
            android:layout_height="300dp" >

            ...

            <LinearLayout
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_alignParentBottom="true"
                android:layout_alignParentRight="true"
                android:layout_margin="4dp"
                android:orientation="vertical" >

                <ImageView
                    android:id="@+id/rejectPhotoImageView"
                    android:layout_width="44dp"
                    android:layout_height="44dp"
                    android:layout_margin="4dp"
                    android:src="@drawable/ic_rechazar2"
                    android:visibility="gone" />

                <ImageView
                    android:id="@+id/takePhotoImageView"
                    android:layout_width="60dp"
                    android:layout_height="60dp"
                    android:layout_margin="4dp"
                    android:src="@drawable/ic_camera2" />
            </LinearLayout>


### Agitar ###

En la clase *"Shake.java"* se utiliza el sensor *"Acelerometro"* para detectar si el dispositivo se agita.

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {


comprueba los tres ejes y si alguno supera uno de los valores máximos se llama a la cámara, mostrando el *preview* para hacer la foto junto a un texto en rojo. Una animación se ejecuta con el mensaje indicando que se ha agitado el dispositivo. Los valores máximos que he fijado son los siguientes:

	// Agitar
	private int MAX_SHAKE_X = 20; 
	private int MAX_SHAKE_Y = 15;  
	private int MAX_SHAKE_Z = 20; 

Mediante un *callback* se informa a *"MainActivity.java"* de que el dispositivo se ha agitado:

    /*
     * Callback de Shake.java.
     * Oye cuando se agita el dispositivo
     */
	@Override
	public void onDidShake() {

		// Animacion subtexto del boton de "publish".
		mShakeTextView.setVisibility(View.VISIBLE);
		Animation anim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.hyperspace_in);
		mShakeTextView.startAnimation(anim);
		Animation anim2 = AnimationUtils.loadAnimation(MainActivity.this, R.anim.hyperspace_out);
		mShakeTextView.startAnimation(anim2);

		openCameraPreview();
	}



En *"onPause()"* de *"MainActivity.java* llamo al método *"unregisterSensor()"* del objeto *mShake* para parar la excucha del acelerómetro.

	if (mShake != null)
    		mShake.unregisterSensor();




### Localización y Geocoding ###


*"LocationFragment.java"* obtiene la localización utilizando tanto *GPS* como *Red*, según disponibilidad:

            gpsLocation = requestUpdatesFromProvider(
                    LocationManager.GPS_PROVIDER, R.string.not_support_gps);
            networkLocation = requestUpdatesFromProvider(
                    LocationManager.NETWORK_PROVIDER, R.string.not_support_network);


La localización se actualiza cada 5 minutos ó 10 metros.

    private static final int UPDATE_METERS = 10;
    private static final int UPDATE_TIME = 1000 * 60 * 5;
    ...
    mLocationManager.requestLocationUpdates(provider, UPDATE_TIME, UPDATE_METERS, listener);

Lee la última localización conocida

        location = mLocationManager.getLastKnownLocation(provider); 


En el método *"getBetterLocation()* se compara la actual localización con la nueva obtenida para seleccionar la mejor:
 
       protected Location getBetterLocation(Location newLocation, Location currentBestLocation) {
       if (currentBestLocation == null) {
          return newLocation;
       } ...



Para versiones a partir de *"GINGERBREAD"* realiza el *"Geocoding*.

        mGeocoderAvailable =
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && Geocoder.isPresent();


Desde un nuevo *hilo* utiliza la clase *"Geocoder"* de la API, y con el método *"getFromLocation()"* a partir de la latitud y longitud obtiene la dirección.

            addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
        }
        ...
            Address address = addresses.get(0);
            addressText = String.format("%s, %s, %s",
                    address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                    address.getLocality(),
                    address.getCountryName());
     
La dirección se comparte con *"MainActivity.java"* a través del  callback *"onSetLocation()"*:

       @Override
       protected void onPostExecute(String resultAddress) {
    	String address = resultAddress;
    	
    	// Actualiza UI
    	mAddressTextView.setText(address);
    	
    	// Callback para actualizar dirección 
    	mLocationListener.onSetLocation(address);


### Fragmentos###

Utiliza llamadas callsbacks para comunicarse con la actividad *"MainActivity.java"*.

Los fragmentos son compatibles con versiones anteriores a Android 3.0 ó nivel 11, utilizando **android.support.v4...**

Lo que me dio un error : 

		The type android.support.v4.app.Fragment cannot be resolved. It is indirectly referenced from required .class files

Lo resolví aplicando la siguiente solución que encontré:

		Its because you might have added the android-support-v4.jar in your project and this library also resides into the facebook SDK also. That is why its throwing this error.
		Right click on the Facebook library => Properties => Java Build Path => Order and Export tab => check android support v4 or Android Private Libraries and select Ok
		Clean both projects, it should work.


### Google Analytics SDK for Android v2 ###


		/*
		 * Métricas
		 */
		mGaInstance = GoogleAnalytics.getInstance(this);
		mGaTracker = mGaInstance.getTracker("UA-47511216-1"); 

		mGaTracker.setStartSession(true);
		mGaTracker.sendView(getClass().getSimpleName());

En *"onCreate()"* de *"MainActivity.java"* envia una *métrica* para recoger en pantallas vista la entrada en la aplicación.

Luego para cada acción del usuario *(hacer login/logout, llamar a la camara, hacer foto, publicar, seleccionar un elemento del listado de fotos,...)* se evía un *"evento"*. Se hace principalmente a través de los callback recibidos en *"MainActivity.java"*. Los *"eventos"* son envíados indicando la *"categoría"*, la *"acción"* y una *"etiqueta"*.


	@Override
	public void onButtonLoginPress() {

		mGaTracker.sendEvent("onButtonLoginPress()", getString(R.string.push), "Login", 0L); 
	}

	@Override
	public void onButtonLogoutPress() {
		mGaTracker.sendEvent("onButtonLogoutPress()", getString(R.string.push), "Logout", 0L);
	}

	@Override
	public void onButtonPublishPress() {

		// Métricas
		mGaTracker.sendEvent("onButtonPublishPress()", getString(R.string.push), "Publicar", 0L);
		...