package com.jmfierro.utad.ejercicio.appiijmfierro;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/** A basic Camera preview class */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

	public void setmCamera(Camera mCamera) {
		this.mCamera = mCamera;
	}

	private SurfaceHolder mHolder;
    private Camera mCamera;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        mCamera = camera;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {

		Log.d(getClass().getSimpleName(), "surfaceCreated()");
    	
//    	if (mCamera != null) {
//    		// The Surface has been created, now tell the camera where to draw the preview.
//    		try {
//    			mCamera.setPreviewDisplay(holder);
//    			mCamera.startPreview();
//    		} catch (IOException e) {
//    			Log.d(CameraPreview.class.getSimpleName(), "Error setting camera preview: " + e.getMessage());
//    		}
//    	}
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
    	 
//    	// Surface will be destroyed when we return, so stop the preview.
//        if (mCamera != null) {
//            // Call stopPreview() to stop updating the preview surface.
//            mCamera.stopPreview();
//        }
    	
		Log.d(getClass().getSimpleName(), "surfaceDestroyed()");

//        if(mCamera!=null){
//        	mCamera.stopPreview();
//        	mCamera.setPreviewCallback(null);
//
//        	mCamera.release();
//        	mCamera = null;
//        }

    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null){
          // preview surface does not exist
          return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
          // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            
            // Sólo en caso de 'portrait'
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) 
            	mCamera.setDisplayOrientation(90);

            // Fija el ratio 1:1 para el Preview.
//            Utils.setOptimalSizeCameraPreview(mCamera, getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);

            mCamera.startPreview();

        } catch (Exception e){
        	Log.d(CameraPreview.class.getSimpleName(),e.getMessage());
        }
    }
    
    public Camera getmCamera() {
		return mCamera;
	}

    
}