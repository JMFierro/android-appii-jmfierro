package com.jmfierro.utad.ejercicio.appiijmfierro;

import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.widget.ProfilePictureView;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;
import com.jmfierro.utad.ejercicio.appiijmfierro.fragmentos.CompassFragment;
import com.jmfierro.utad.ejercicio.appiijmfierro.fragmentos.FacebookFragment;
import com.jmfierro.utad.ejercicio.appiijmfierro.fragmentos.ListPhotosFragment;
import com.jmfierro.utad.ejercicio.appiijmfierro.fragmentos.LocationFragment;
import com.jmfierro.utad.ejercicio.appiijmfierro.fragmentos.MapGoogleFragment;
import com.jmfierro.utad.ejercicio.appiijmfierro.utils.UtilsCamera;
import com.jmfierro.utad.ejercicio.appiijmfierro.utils.UtilsMultimedia;

public class MainActivity extends FragmentActivity 
						implements 
						LocationFragment.OnLocationListener, 
						FacebookFragment.OnFecebookListener,
						CompassFragment.OnOrientationListener,
						ListPhotosFragment.OnListPhotosListener, 
						Shake.OnShakeListener { 

	
	// Location y geocoding
	private LocationFragment mLocationFrag;
	private String mAddress;
	private String mOrientation;
	
	// Agitar
	private int MAX_SHAKE_X = 20; 
	private int MAX_SHAKE_Y = 15;  
	private int MAX_SHAKE_Z = 20; 
	private Shake mShake;
	private TextView mShakeTextView;


	// Métricas
	private Tracker mGaTracker;
	private GoogleAnalytics mGaInstance;

	// Camara
//	private Button mCamaraButton;
	private ImageView mUpdatePhotoImageView;
	private ImageView mPhotoImageView, mTakePhotoImageView, mRejectPhotoImageView;
	private Bitmap mPhoto = null;
//	private Button mPhotoButton;
	private String mMsgPhoto;
    private Camera mCamera = null;
    private CameraPreview mPreview = null;
    private FrameLayout mPreviewCameraFrameLayout;
    public static final int CAMERA_ID = 0;

    //ListPhotos
    private String[]  mAddressArrayString;
    private String[]  mPhotosArrayString ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Log.d(getClass().getSimpleName(), "onCreate()");

		
		/*
		 * Métricas
		 */
		mGaInstance = GoogleAnalytics.getInstance(this);
		mGaTracker = mGaInstance.getTracker("UA-47511216-1"); 

		mGaTracker.setStartSession(true);
		mGaTracker.sendView(getClass().getSimpleName()); 

		
		// Cámara
		mTakePhotoImageView = (ImageView) findViewById(R.id.takePhotoImageView);
		mRejectPhotoImageView = (ImageView) findViewById(R.id.rejectPhotoImageView);
		mUpdatePhotoImageView = (ImageView) findViewById(R.id.updatePhotoImageView);
		mPreviewCameraFrameLayout = (FrameLayout) findViewById(R.id.camera_preview);
		

		// Localizacion
		mLocationFrag = (LocationFragment) getSupportFragmentManager().findFragmentById(R.id.localizacionFragment);


//		// Mapa posicion actual y zoom.
//    	MapGoogleFragment map = (MapGoogleFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
//    	map.setLocationMap(mLocationFrag.getLatitud(), mLocationFrag.getLongitud());

		// Acelerometro
		mShakeTextView  =(TextView) findViewById(R.id.shakeTextView);
		
        
        // Listado fotos
        updateListPhotosFragment();
        
		// Cámara
		updateButtonsCamera(false);
		mCamera = startCamera();
		mPreviewCameraFrameLayout.setVisibility(View.GONE); 

		
		
		mUpdatePhotoImageView.setOnClickListener(new OnClickListener() {
			
			private List<Size> mSupportedPreviewSizes;

			@Override
			public void onClick(View v) {
				mGaTracker.sendEvent("mUpdatePhotoImageView", getString(R.string.push),  getString(R.string.camara_update_photo), 0L);
				openCameraPreview();
			}
		});

	}
	
	
	
	public void scaleView(View v, float startScale, float endScale) {
	    Animation anim = new ScaleAnimation(
	            1f, 1f, // Start and end values for the X axis scaling
	            startScale, endScale, // Start and end values for the Y axis scaling
	            Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
	            Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
	    anim.setFillAfter(true); // Needed to keep the result of the animation
	    v.startAnimation(anim);
	}

	
	

	
	@Override
	protected void onStart() {
		super.onStart();

		Log.d(getClass().getSimpleName(), "onStart()");
		

	}
	
	@Override
	protected void onResume() {
		super.onResume();

		Log.d(getClass().getSimpleName(), "onResume()");

        // Acelerometro
		mShake = new Shake(this, MAX_SHAKE_X, MAX_SHAKE_Y, MAX_SHAKE_Z);

		
//		if (mCamera != null && mPreview.getmCamera() != null) {
//			try {
//				mCamera.reconnect();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
////			mCamera.startPreview();
//		}
		
	}
	
	
	@Override
	protected void onPause() {

		Log.d(getClass().getSimpleName(), "onPause()");

    	if (mShake != null)
    		mShake.unregisterSensor();
		
        if(mCamera!=null){
        	mCamera.stopPreview();
        	mCamera.setPreviewCallback(null);

        	mCamera.release();
        	mCamera = null;
        }

        
        
		super.onPause();

	}
	

	@Override
	protected void onStop() {

		super.onStop();

		Log.d(getClass().getSimpleName(), "onStop()");

		
		// Manda  toda la info que queda por enviar.
////		EasyTracker.getInstance().dispatch();
////		EasyTracker.getInstance().activityStop(this);
//		mGaTracker.setStartSession(false);
		
//		try {
//			mCamera.reconnect();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block 
//			e.printStackTrace();
//		}
		
	}
	
	
	@Override
	protected void onDestroy() {

		Log.d(getClass().getSimpleName(), "onDestroy()");

		// Facebook
//		mFacebook.doLogout();
//		doLogout();

		/*
		 * Metricas
		 */
//		mGaTracker.setStartSession(true);
		mGaTracker.sendView("onDestroy->/" + getClass().getSimpleName()); 

		mGaInstance.closeTracker(mGaTracker);
		mGaTracker.close();  // = mGaInstance.getTracker("UA-47511216-1");
		
//        if(mCamera!=null){
//        	mCamera.stopPreview();
//        	mCamera.setPreviewCallback(null);
//
//        	mCamera.release();
//        	mCamera = null;
//        }
////        stopPreviewAndFreeCamera();
		

		super.onDestroy();
	}
	
	


	
	/**-------------------------------------------
	 * Callback de LocationFragment.
	 * Obtiene y guarda la longitud y la latitud
	 ---------------------------------------------*/
	@Override
	public void onSetLocation(String address) {
		mAddress = address;

		// Mapa posicion actual y zoom.
    	MapGoogleFragment map = (MapGoogleFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
    	map.setLocationMap(mLocationFrag.getLatitud(), mLocationFrag.getLongitud());
	}

	@Override
	public void onSetOrientation(String orientation) {
		
		mOrientation = orientation;
	}

	
	
    
    /**------------------------------------------------
     * 
     *** Métodos para la utilización de la Cámara.  ***
     * 
     -------------------------------------------------*/
	private PictureCallback onSetCameraCallback = new PictureCallback() {

        @Override
        public void onPictureTaken(final byte[] data, final Camera camera) {

        
        	/*
        	 * Actualiza UI
        	 */
        	updateButtonsCamera(false);
        	

        	
        	/*
        	 * Se escucha si se agita el dispositivo.
        	 */
        	if (mShake != null)
        		mShake.registerSensor();
        	else
        		mShake = new Shake(MainActivity.this, MAX_SHAKE_X, MAX_SHAKE_Y, MAX_SHAKE_Z);

			/**
        	// Lectura de parametros de la camara
        	float horizontalViewAngleCamera = camera.getParameters().getHorizontalViewAngle();
        	float vertitcalViewAngleCamera = camera.getParameters().getVerticalViewAngle();
        	*/


        	/*
        	 * Se guarda el mensage que se subira con la foto.
        	 */
    		mMsgPhoto = mAddress 
        			+ "\n" + "(" + mOrientation +")";
//        			+ "\nhorizontalViewAngleCamera: " + horizontalViewAngleCamera
//        			+"\nvertitcalViewAngleCamera: " + vertitcalViewAngleCamera;

    		MediaPlayer.create(getApplicationContext(), R.raw.camera_shutter_click_08).start();

    		
    		/*
    		 * Hilo para obtener el bitmpa, manipularlo y salvarlo en fichero.
    		 */
    		
        	new Thread(new Runnable() {
				
        		
        		/* -------------------------------------------------------
        		 * 
        		 * La manipulación de "bitmap" y el salvado en fichero se hace
        		 * en un hilo aparte, para que no interfiera con la UI.!)
        		 * 
        		 ----------------------------------------------------------*/
				@Override
				public void run() { // ===========(on)=======[Thread(new Runnable() - run()]=============================
					
		            /*
		             * Obtiene bitmap
		             */
		        	// Lectura de parametros de la camara
					/**
		        	float horizontalViewAngleCamera = camera.getParameters().getHorizontalViewAngle();
		        	float vertitcalViewAngleCamera = camera.getParameters().getVerticalViewAngle();
		        	*/


		    		mMsgPhoto = mAddress 
		        			+ "\n" + "(" + mOrientation +")";
//		        			+ "\nhorizontalViewAngleCamera: " + horizontalViewAngleCamera
//		        			+"\nvertitcalViewAngleCamera: " + vertitcalViewAngleCamera;
		        	Bitmap photo = BitmapFactory.decodeByteArray(data, 0, data.length);
		        	
		        	
		            /*
		             * Orientación correcta de la foto obtenida.
		             */
		        	int orientation = UtilsCamera.getOrientationAlrightOfPhothoCamera(MainActivity.this, CAMERA_ID, camera);
		        	photo = UtilsMultimedia.getBtimapRotate(photo, orientation);
		        	
		        	/*
		        	 *  Sólo para camara frontal. Invierte la imagen (efecto espejo)
		        	 */
//		        	android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
//		            camera.getCameraInfo(CAMERA_ID, info);
//		        	if (info.facing == CameraInfo.CAMERA_FACING_FRONT)
//		        		photo = UtilsMultimedia.getBitmapMirror(photo);

		        	mPhoto = photo;
					FacebookFragment facebookFrang = (FacebookFragment) getSupportFragmentManager().findFragmentById(R.id.facebookFragment);
					facebookFrang.setPhoto(photo);
					facebookFrang.setMsgPhoto(mMsgPhoto);
					
		            UtilsMultimedia.saveFile(MainActivity.this, photo);
		            
		            /*
		             * Actualiza UI
		             */
		            runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							
							FacebookFragment facebookFrang = (FacebookFragment) getSupportFragmentManager().findFragmentById(R.id.facebookFragment);
							facebookFrang.updateButtonPublish(true);
							
							updateListPhotosFragment();
							
				        	MapGoogleFragment map = (MapGoogleFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
				        	map.setImageMap(UtilsMultimedia.getRoundedShape(mPhoto), mMsgPhoto,mLocationFrag.getLatitud(),mLocationFrag.getLongitud());

						}
					});

		            
		            // Libera la camara
		            /*
		            mCamera.release();
		            */

					
				} // =============(off)=====[Thread(new Runnable() - run()]===================================
			}).start();
        	/*---------------------------------------------------
        	 * 
        	 * Fin hilo : ==(off)=====[Thread(new Runnable() - run()]=====
        	 * 
        	 -----------------------------------------------------*/
        	
        	


        	/*
        	 * Crea tumbnails
        	 */
        	/**
        	//        	private void setPic() {
        	try     
        	{

        		//                final int THUMBNAIL_SIZE = 64;
        		final int THUMBNAIL_SIZE = 500;
        		Bitmap photoTumbails = Bitmap.createScaledBitmap(photo, THUMBNAIL_SIZE, THUMBNAIL_SIZE, false);

        		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
        		photoTumbails.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        		byte[] imageData = baos.toByteArray();

        		//                photo = photoTumbails;
        		photo2.setImageBitmap(photoTumbails);

        	}
        	catch(Exception ex) {

        	}
        	**/

        	
            
        	/*
        	 * Guarda bitmap en jpg
        	 */
        	/**
            File pictureFile = UtilsMultimedia.getOutputMediaFile(1);  //MEDIA_TYPE_IMAGE);
            if (pictureFile == null){
                Log.d(MainActivity.class.getSimpleName(), "Error creating media file, check storage permissions");
                return;
            }

            String pathString = pictureFile.getPath();
            String nameString = pictureFile.getName();
            
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] photoByteArray = stream.toByteArray();
                fos.write(photoByteArray);
                
                
                                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
            	Log.d(MainActivity.class.getSimpleName(), "File not found: " + e.getMessage());
            } catch (IOException e) {
            	Log.d(MainActivity.class.getSimpleName(), "Error accessing file: " + e.getMessage());
            }

        	 **/            
        	
        	
        	
            
            /*
             *  Forma (I)
             *  Añadiendo direccion con "ExifInterface.java"
             */
        	/**
        	File pictureFile = UtilsMultimedia.getOutputMediaFile(MainActivity.this, 1);  //MEDIA_TYPE_IMAGE);
            ExifInterface exifJava = null;
			try {
				exifJava = new ExifInterface(pictureFile.getPath()); 
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
            exifJava.setAttribute("UserComment", mAddress);
            try {
				exifJava.saveAttributes();
			} catch (IOException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
            Log.d("Files", "ExifInterface" + exifJava.getAttribute("UserComment"));

            
            AssetManager am = getResources().getAssets();
            Properties pp = new Properties();

            InputStream isConfig = null;
			try {
				isConfig = am.open("config.properties",Context.MODE_PRIVATE);
	            pp.load(isConfig);
			} catch (IOException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}           

            pp.setProperty("SHOP_URL", "NEW_SHOP_URL");//This key exists

            try {
                pp.store(new FileOutputStream("config.properties"), null);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }catch (IOException e) {
                e.printStackTrace();
            }
        	 **/



        	/*
        	 * Forma (II)
        	 *  Añadir direccion a file jpg
        	 */
        	/**
        	String mString = "Your message here";     
        	ExifInterface exif;
			try {
				exif = new ExifInterface(pictureFile.getPath());
                Log.d("Files", "pictureFile.getPath()" + pictureFile.getPath());
				// exif = new ExifInterface(getFilePhotos().getPath());
	        	exif.setAttribute("UserComment", mAddress);
	        	exif.saveAttributes();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}


            // ((ImageView) findViewById(R.id.photo1))
            //	.setImageBitmap(UtilsMultimedia.readFile(pathString));
        	 * 
        	 * 
        	 */

        	/*
        	 * Lee ficheros del directorio picture.
        	 */
        	/**
        	File file = new File(Environment.getExternalStoragePublicDirectory(
        			Environment.DIRECTORY_PICTURES), MainActivity.this.getString(R.string.app_name));
        	//            File file = getFilePhotos();
        	File[] pictures = file.listFiles();
        	for (int i=0; i < pictures.length; i++)
        	{
        		Log.d("Files", "FileName:" + pictures[i].getPath());

<

        		//                // Añadir direccion a file jpg
        		//            	ExifInterface exif1;
        		//    			try {
        		//    				exif1 = new ExifInterface(pictures[i].getPath());
        		//                    Log.d("Files", "pictureFile.getPath()" + pictures[i].getPath());
        		////    				exif = new ExifInterface(getFilePhotos().getPath());
        		//    	        	exif1.setAttribute("UserComment", Integer.toString(i));
        		//    	        	exif1.saveAttributes();
        		//
        		//                    String addressString = exif1.getAttribute("UserComment");
        		//                    Log.d("Files", "Recien escrito:" + addressString);
        		//
        		//    			} catch (IOException e1) {
        		//    				// TODO Auto-generated catch block
        		//    				e1.printStackTrace();
        		//    			}



        		// Abre file
        		OutputStream out = null;
        		try {
        			out = new FileOutputStream(pictures[i].getPath());
        		} catch (FileNotFoundException e1) {
        			// TODO Auto-generated catch block
        			e1.printStackTrace();
        		}



        		// Añadiendo direccion con "ExifInterface.java"
        		ExifInterface exifJava1 = null;
        		try {
        			exifJava1 = new ExifInterface(pictures[i].getPath());
        		} catch (IOException e2) {
        			// TODO Auto-generated catch block
        			e2.printStackTrace();
        		}
        		//                exifJava1.setAttribute("UserComment", 
        		//                		"(" +Integer.toString(i) + ")" 
        		//                		+ "-" + mAddress);
        		//                try {
        		//					exifJava1.saveAttributes();
        		//				} catch (IOException e1) {
        		//					// TODO Auto-generated catch block
        		//					e1.printStackTrace();
        		//				}
        		Log.d("Files", "ExifInterface" + exifJava1.getAttribute("UserComment"));



        		// Cierra fichero.
        		try {
        			out.close();
        		} catch (IOException e1) {
        			// TODO Auto-generated catch block
        			e1.printStackTrace();
        		}



        		// Leer dirección
        		String filename = pictures[i].getPath();
        		try {
        			ExifInterface exif11 = new ExifInterface(filename);
        			String addressString = exif11.getAttribute("UserComment");
        			Log.d("Files", "Dirección: " + addressString);
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        			Toast.makeText(MainActivity.this, "Error!", Toast.LENGTH_LONG).show();
        		}

        	}
            
        	 **/  
        	
        	
        	
            /*
             * Actualiza fragment 
             */
        	/**
            ListPhotosFragment listPhotosFrang = new ListPhotosFragment();
            FragmentManager fragmento =
            **/ 
            
        }
    };

    
    
    
    
    
    
    /**
     * Inicia la camara y activa preview
     */
    private Camera startCamera() {
    	
    	
    	/*
    	 * Gestión desde MainActivity.java,
    	 * ya depurada. 
    	 *
    	 */
		try {
			// Reiniciliza la camara si esta ocupada o bloqueada.
			
			
			// Nueva camara
			final Camera camera = Camera.open(CAMERA_ID); // attempt to get a Camera instance
			mPreviewCameraFrameLayout.setVisibility(View.VISIBLE);

			
			/*
			 * Orientación para el preview.
			 * 
			 * Tiene en cuenta cuatro posiciones:
			 *    Portrat (derecho y la reves)
			 *    Landscape (derecho y la reves)
			 */
        	UtilsCamera.setOrientationCameraPreview(MainActivity.this, CAMERA_ID, camera);

			// Crea un preview
	        mPreview = new CameraPreview(this, camera);
	        mPreviewCameraFrameLayout.addView(mPreview);
	    
	        // Listener para el boton de foto.
	        ImageView captureButton = (ImageView) findViewById(R.id.takePhotoImageView);
	        captureButton.setOnClickListener(
	            new View.OnClickListener() {
	                @Override
	                public void onClick(View v) {
	            		// Métrica
	            		mGaTracker.sendEvent("takePhotoImageView", getString(R.string.push), getString(R.string.camara_take_photo), 0L);
	                	camera.takePicture(null, null, onSetCameraCallback);
	                }
	            }
	        );

	        // Listener para el boton de foto.
	        final ImageView rejectButton = (ImageView) findViewById(R.id.rejectPhotoImageView);
//	        rejectButton.setVisibility(View.VISIBLE);
	        rejectButton.setOnClickListener(
	            new View.OnClickListener() {
	                @Override
	                public void onClick(View v) {
	                    if(mCamera!=null){
	                    	mCamera.stopPreview();
	                    }
	            		updateButtonsCamera(false);
	            		mPreviewCameraFrameLayout.setVisibility(View.GONE);
	            		ImageView cameraPreviewPhoto  = (ImageView) findViewById(R.id.camera_preview_photo);
	            		cameraPreviewPhoto.setVisibility(View.INVISIBLE);
	                    
	                    // Acelerometro
	                    if (mShake != null)
	                		mShake.unregisterSensor();
	            		mShake = new Shake(MainActivity.this, MAX_SHAKE_X, MAX_SHAKE_Y, MAX_SHAKE_Z);

	                    // Métrica
	            		mGaTracker.sendEvent("rejectPhotoImageView", getString(R.string.push), getString(R.string.camara_reject_photo), 0L);
	                }
	            }
	        );

	        
	        return camera;

	    }
	    catch (Exception e){ 
	        // Camera is not available (in use or does not exist)
	    	String msgError = getString(R.string.error_camera) + e.toString() ;
	    	Log.d(MainActivity.class.getSimpleName(),msgError);
	    	Toast.makeText(this, msgError, Toast.LENGTH_LONG).show();

	    	return null;
	    }
    }
    
    
    
    /**
     * Abre el preiview para hacer la foto
     */
    public void openCameraPreview() {

    	if (mShake != null)
    		mShake.unregisterSensor();

    	MediaPlayer.create(getApplicationContext(), R.raw.camera_shutter_click_01).start();

    	updateButtonsCamera(true);
    	mPreviewCameraFrameLayout.setVisibility(View.VISIBLE);

    	if (mCamera == null)
    		mCamera = startCamera();
    	else
    		mCamera.startPreview();
    }
    
    

    
	/**
	 * Callback de la clase ListPhotosFragment
	 */
	@Override
	public void onSetPhotoForPublish(Bitmap photoForPublis) {
			mPhoto = photoForPublis;
			mMsgPhoto = mAddress 
					+ "\n" + "(" + mOrientation +")";
			
			FacebookFragment facebookFrang = (FacebookFragment) getSupportFragmentManager().findFragmentById(R.id.facebookFragment);
			facebookFrang.setPhoto(mPhoto);
			facebookFrang.setMsgPhoto(mMsgPhoto);
			facebookFrang.updateButtonPublish(true);
		
	}
   
	@Override
	public void onClickItem() {
        // Métrica
		mGaTracker.sendEvent(getString(R.string.list_photos), getString(R.string.push), getString(R.string.list_photos_click_item), 0L);
		
	}
    


    
    
	
	/**---------------------------------------------------
	 * 
	 * Métodos para la actualizacion de la UI
	 * 
	 -----------------------------------------------------*/
	
	
	public void updateListPhotosFragment() {
    	
		ListPhotosFragment listPhotosFrang = new ListPhotosFragment();
		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.listPhotosFragment, listPhotosFrang)
				.commit();
    }

	
    public void updateButtonsCamera(boolean enableBoolean) {

    	/*
    	 * Botones camara
    	 */
    	if (enableBoolean) {
    		mTakePhotoImageView.setEnabled(true);
    		mTakePhotoImageView.setVisibility(View.VISIBLE);
    		mRejectPhotoImageView.setEnabled(true);
    		mRejectPhotoImageView.setVisibility(View.VISIBLE);
    		mUpdatePhotoImageView.setEnabled(false);
    		mUpdatePhotoImageView.setVisibility(View.GONE);
    	}
    	else {
    		mTakePhotoImageView.setEnabled(false);
    		mTakePhotoImageView.setVisibility(View.GONE);
    		mRejectPhotoImageView.setEnabled(false);
    		mRejectPhotoImageView.setVisibility(View.GONE);
    		mUpdatePhotoImageView.setEnabled(true);
    		mUpdatePhotoImageView.setVisibility(View.VISIBLE);
    		
    		mShakeTextView.setVisibility(View.GONE);
    	}
    }

    
    


    /*
     * Callback de Shake.java.
     * Oye cuando se agita el dispositivo
     */
	@Override
	public void onDidShake() {

		// Animacion subtexto del boton de "publish".
		mShakeTextView.setVisibility(View.VISIBLE);
		Animation anim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.hyperspace_in);
		mShakeTextView.startAnimation(anim);
		Animation anim2 = AnimationUtils.loadAnimation(MainActivity.this, R.anim.hyperspace_out);
		mShakeTextView.startAnimation(anim2);

		openCameraPreview();
	}




    
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
//		mFacebook.getActiveSession().getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		FacebookFragment facebookFrang = (FacebookFragment) getSupportFragmentManager().findFragmentById(R.id.facebookFragment);
		facebookFrang.getActiveSession().getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		
	}



	/**------------------------------
	 * 
	 * Callbacks de Facebook.java
	 * 
	 --------------------------------*/
	@Override
	public void onSessionOpened() { 

		FacebookFragment facebookFrang = (FacebookFragment) getSupportFragmentManager().findFragmentById(R.id.facebookFragment);
		facebookFrang.updateButtonPublish(true);

		// Métrica
		mGaTracker.sendEvent(getString(R.string.facebook), "onSessionOpened()", getString(R.string.sesion_facebook_abierta), 0L);
		Log.d(MainActivity.class.getSimpleName(), getString(R.string.sesion_facebook_abierta));

////	    ProfilePictureView profilePicture = (ProfilePictureView) view.findViewById(R.id.profilePicture);
//	    mPprofilePicture.setProfileId(userID);
	}



	@Override
	public void onCompleted(String msgResponse) {
		String msg = getString(R.string.uplodad_facebook_ok) 
				+"\n"
				+"\n msgResponse:"
				+ msgResponse;
		
		Log.e(MainActivity.class.getSimpleName(), msg);
		
		// Métrica
		mGaTracker.sendEvent(getString(R.string.facebook), "onCompleted()", msg, 0L);

		Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
		
	}



	@Override
	public void onSessionException(String masException) {
		
		String msg = getString(R.string.error_hacer_login_fecebook) + masException;
		Log.e(MainActivity.class.getSimpleName(), msg);

		// Métrica
		mGaTracker.sendEvent(getString(R.string.facebook), "onSessionException()", msg, 0L);
		
	}



	@Override
	public void onCallReintent() {
		
		String msg = getString(R.string.uplodad_facebook_reintent); 
		Log.e(MainActivity.class.getSimpleName(), msg);

		// Métrica
		mGaTracker.sendEvent(getString(R.string.facebook), "onCallReintent()", msg, 0L);
		
	}



	@Override
	public void onCallException(String msgException) {
		String msg = getString(R.string.uplodad_facebook_exception) + msgException; 
		Log.e(MainActivity.class.getSimpleName(), msg);
		
		// Métrica
		mGaTracker.sendEvent(getString(R.string.facebook), "onCallException()", msg, 0L);
		
	}



	@Override
	public void onButtonLoginPress() {

		mGaTracker.sendEvent("onButtonLoginPress()", getString(R.string.push), "Login", 0L); 
		
	}



	@Override
	public void onButtonLogoutPress() {
		mGaTracker.sendEvent("onButtonLogoutPress()", getString(R.string.push), "Logout", 0L);
		
	}



	@Override
	public void onButtonPublishPress() {

		// Métricas
		mGaTracker.sendEvent("onButtonPublishPress()", getString(R.string.push), "Publicar", 0L);

		// Animacion fotografía publicanda en Facebook. 
		mPreviewCameraFrameLayout.setVisibility(View.GONE);
		ImageView cameraPreviewPhoto  = (ImageView) findViewById(R.id.camera_preview_photo);
		cameraPreviewPhoto.setVisibility(View.VISIBLE);
		cameraPreviewPhoto.setImageBitmap(mPhoto);
		Animation anim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.hyperspace_out);
		cameraPreviewPhoto.startAnimation(anim);
		cameraPreviewPhoto.setVisibility(View.INVISIBLE);

		mPhoto = null;

		
	}



	
/**------------------------------
 * 
 * Animaciones ejemplos 
 * 
 --------------------------------*/
    
//	Animator set = (AnimatorSet) AnimatorInflater.loadAnimator(context, id)Animator(MainActivity.this, R.anim.slide_out);
//	set.setTarget(mPublishTextView);
//	set.start();
	
//	Animation animation = new TranslateAnimation(0, 0,0, 100);
//	animation.setDuration(2000);
//	animation.setFillAfter(true);
//	mPublishTextView.startAnimation(animation); 
////	mPublishTextView.setVisibility(0);

	
//	scaleView(mPublishTextView, 0f, 0.6f);

	
//	 android:fromXScale="4" android:toXScale="1" android:fromYScale="3"
//	 android:toYScale="1" android:pivotX="10%" android:pivotY="80%"
//	 android:duration="2000" />

//    Animation anim = new ScaleAnimation(
//            4f, 1f, // Start and end values for the X axis scaling
//            3f, 1f, // Start and end values for the Y axis scaling
//            Animation.RELATIVE_TO_SELF, 10f, // Pivot point of X scaling
//            Animation.RELATIVE_TO_SELF, 80f); // Pivot point of Y scaling
//    anim.setFillAfter(true); // Needed to keep the result of the animation
//    mPublishTextView.startAnimation(anim);

    
}