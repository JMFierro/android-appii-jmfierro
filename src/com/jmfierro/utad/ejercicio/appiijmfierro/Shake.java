package com.jmfierro.utad.ejercicio.appiijmfierro;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.TextView;

public class Shake implements SensorEventListener {

	private SensorManager mSensorManager;
	private TextView[] textViews;
	
	private float maxDidX,maxDidY,maxDidZ;
	private float maxX,maxY,maxZ;
	private Context context;
	
	/**
	 * Callback
	 */
	private OnShakeListener mShakeListener;
	private static OnShakeListener CallbacksVacios = new OnShakeListener() {
		@Override
		public void onDidShake() {
		}
	};
	
	public interface OnShakeListener { 
		public void onDidShake ();
	}
	
	
	public Shake(Context context, float maxX, float maxY, float maxZ) {
		this.context = context;
		this.maxX = maxX;
		this.maxY = maxY;
		this.maxZ = maxZ;
		
        maxDidX=0;
        maxDidY=0;
        maxDidZ=0;
        
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

//        mSensorManager.registerListener(this,
//        		mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
//        		SensorManager.SENSOR_DELAY_NORMAL);
        registerSensor();
        
        mShakeListener = (OnShakeListener) context;
	}
	

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

			float x = Math.abs(event.values[0]);
			float y = Math.abs(event.values[1]);
			float z = Math.abs(event.values[2]);
			
			
			if (x >= maxX || y >= maxY || z >= maxZ) {
				maxDidX = maxDidY = maxDidZ = 0;
				mShakeListener.onDidShake(); 
				//context.getClass().onDidShake(); 
			}
			else {

				if (x > maxDidX)
					maxDidX = x;

				if (y > maxDidY) 
					maxDidY = y;

				if (z > maxDidZ)
					maxDidZ = z;
			}
		}
		
	}
    
//    @Override
//    protected void onStop() {
//        mSensorManager.unregisterListener(this);
//        super.onStop();
//    }

    
	public void registerSensor() {
        mSensorManager.registerListener(this,
        		mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
        		SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	public void unregisterSensor() {
      mSensorManager.unregisterListener(this);
	}
}
