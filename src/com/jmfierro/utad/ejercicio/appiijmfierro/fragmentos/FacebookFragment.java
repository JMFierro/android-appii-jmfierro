package com.jmfierro.utad.ejercicio.appiijmfierro.fragmentos;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Session.StatusCallback;
import com.facebook.android.Facebook;
import com.facebook.model.GraphObject;
import com.facebook.widget.ProfilePictureView;
import com.jmfierro.utad.ejercicio.appiijmfierro.R;
import com.jmfierro.utad.ejercicio.appiijmfierro.R.anim;
import com.jmfierro.utad.ejercicio.appiijmfierro.R.drawable;
import com.jmfierro.utad.ejercicio.appiijmfierro.R.id;
import com.jmfierro.utad.ejercicio.appiijmfierro.R.layout;
import com.jmfierro.utad.ejercicio.appiijmfierro.R.string;
import com.jmfierro.utad.ejercicio.appiijmfierro.utils.UtilsNet;


public class FacebookFragment extends Fragment {

	private static final String PUBLISH_PERMISSION = "publish_actions";
	private static final String FACEBOOK_SESSION = "facebook-session";
	private static final String FACEBOOK_SESSION_LOGIN = "fecebook-login";
	private static final String FACEBOOK_SESSION_ACCES_TOKEN = "access_token";
	private Context mContext;
	private Session mSession;
	
	// Facebbok
//	private FacebookClass mFacebook;
//	private static final String PUBLISH_PERMISSION = "publish_actions";
	private ProfilePictureView mProfilePicture;
	private Button mLoginButton;
	private ImageView mPublishImageView;
	private TextView mPublishTextView;
	private TextView mVoidTextView;

	private Bitmap mPhoto;
	private String mMsgPhoto;
	
	/*
     * Callback
     */
    private OnFecebookListener mFecebookListener;

	private Object mOrientation;
    
    public interface OnFecebookListener {
    	public void onSessionOpened ();
    	public void onCompleted(String msgResponse);
    	public void onSessionException(String msgException);
    	public void onCallReintent();
    	public void onCallException(String msgException);
    	public void onButtonLoginPress();
    	public void onButtonLogoutPress();
    	public void onButtonPublishPress();
    }
    
    private static OnFecebookListener CallbackVacios = new OnFecebookListener() {
		
		@Override
		public void onSessionOpened() {
			
		}

		@Override
		public void onSessionException(String msgException) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onCompleted(String msgResponse) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onCallReintent() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onCallException(String msgException) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onButtonLoginPress() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onButtonLogoutPress() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onButtonPublishPress() {
			// TODO Auto-generated method stub
			
		}

	};
	
	/**
	 * Constructor
	 */
	public FacebookFragment() {
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.mContext = getActivity();
		
		Session.StatusCallback statusCallback = new StatusCallback() {
			
			@Override
			public void call(Session session, SessionState state, Exception exception) {
//				updateUi();
			}
		};

		
		if (UtilsNet.isNet(mContext)) {

			Session session = new Session(mContext);
			Session.setActiveSession(session);

		}
		
		// Lee de preferencias si el usuario salio la última vez en "login" o "logout".
		if (this.isDidLogin())
			this.doLogin();
	

	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.facebook_fragment, container, false);
	
	    mProfilePicture = (ProfilePictureView) view.findViewById(R.id.profilePicture);
		mLoginButton = (Button) view.findViewById(R.id.loginButton);
		mPublishImageView = (ImageView) view.findViewById(R.id.facebook_publishIamgenView);
		mPublishTextView = (TextView) view.findViewById(R.id.publishTextView);
//		mVoidTextView = (TextView) findViewById(R.id.voidTextView);

		this.updateButtonPublish(false);
		

		/*
		 * Listeners para botones.
		 */
		
		// Login button
		mLoginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Session session = FacebookFragment.this.getActiveSession();
				if (session != null) 
					if(!session.isOpened() ) {
						FacebookFragment.this.mFecebookListener.onButtonLoginPress(); 
						FacebookFragment.this.doLogin();
						updateButtonPublish(true);
					}

					else {
						FacebookFragment.this.mFecebookListener.onButtonLogoutPress(); 
						FacebookFragment.this.doLogout();
						
						// Guarda que el usuario hace logout.
						FacebookFragment.this.saveDidLogin(false);  
						updateButtonPublish(false);
					}
			}
		});
		
		
		// publish button
		mPublishImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mFecebookListener.onButtonPublishPress();
				FacebookFragment.this.doPublish(mPhoto, mMsgPhoto);
				FacebookFragment.this.updateButtonPublish(false);
			}
		});



		
		return view;
	}
	

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
    	if(!(activity instanceof OnFecebookListener)) {
    		throw new IllegalStateException(getString(R.string.error_on_facebooklisterner));
    	}
    	
    	mFecebookListener = (OnFecebookListener) activity;

	}
	
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		mFecebookListener = CallbackVacios;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);  
		
		this.getActiveSession().onActivityResult((Activity)mContext, requestCode, resultCode, data);

	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		outState = FacebookFragment.this.saveSessionInstanceState(outState);
		
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		this.doLogout();
	}
	
	public void doLogin() {
		
		/*
		 * Sólo si hay conexión a Internet.
		 */
		if (UtilsNet.isNet(mContext)) {

//			Session session = new Session(mContext);
//			Session.setActiveSession(session);
			Session session = Session.getActiveSession();
			StatusCallback statusCallback = new StatusCallback() {
					@Override
					public void call(Session session, SessionState state, Exception exception) {
						if (SessionState.OPENED.equals(state)) {
							
							executeMeRequest(session);
							
							saveDidLogin(true);
							mFecebookListener.onSessionOpened();
							
							//						updateButtonPublish(true);
							//						
							//						// Métrica
							//						mGaTracker.sendEvent("doLogin()", "OPENED", getString(R.string.sesion_facebook_abierta), 0L);

						}
						else if (exception != null) {
							mFecebookListener.onSessionException(exception.getMessage());

							//							String msg = mContext.getString(R.string.error_hacer_login_fecebook) + exception.getMessage();
							//							Log.e(MainActivity.class.getSimpleName(), msg);
							//
							//							// Métrica
							//							mGaTracker.sendEvent("doLogin()", "exception", msg, 0L);

						}
					}
				};

			    if (!session.isOpened() && !session.isClosed()) {
			    	
			    	Session.OpenRequest request = new Session.OpenRequest((Activity) mContext);
					request.setCallback(statusCallback);
					session.openForRead(request);
			    }
			    else {
			    	Session.openActiveSession((Activity) mContext, true, statusCallback);
			    }

			
//			if ( !session.isOpened() ) {
//				Session.OpenRequest request = new Session.OpenRequest((Activity) mContext);
//				
//				request.setCallback(new StatusCallback() {
//					@Override
//					public void call(Session session, SessionState state, Exception exception) {
//						if (SessionState.OPENED.equals(state)) {
//							
//							executeMeRequest(session); 
//							
//							saveDidLogin(true);
//							mFecebookListener.onSessionOpened();
//							
//							//						updateButtonPublish(true);
//							//						
//							//						// Métrica
//							//						mGaTracker.sendEvent("doLogin()", "OPENED", getString(R.string.sesion_facebook_abierta), 0L);
//
//						}
//						else if (exception != null) {
//							mFecebookListener.onSessionException(exception.getMessage());
//
//							//							String msg = mContext.getString(R.string.error_hacer_login_fecebook) + exception.getMessage();
//							//							Log.e(MainActivity.class.getSimpleName(), msg);
//							//
//							//							// Métrica
//							//							mGaTracker.sendEvent("doLogin()", "exception", msg, 0L);
//
//						}
//					}
//				});
//
//				session.openForRead(request);
//				//			session.openForPublish(request);
//			}
//			else {
//				//			updateUi();
//				
//			}
		}
	}

	
	
	public void doLogout() {
		////		Session.getActiveSession().close();
		//		Session.getActiveSession().closeAndClearTokenInformation();

		Session session = Session.getActiveSession();
		if (session != null) {
			
			session.close();
			session.closeAndClearTokenInformation();
		}
		
	}

	
	public void doPublish(final Bitmap bitmap, final String msgPhoto) {
		
		Session session = Session.getActiveSession();
		
		/*
		 * No hay permiso !!!
		 */
		if ( !session.getPermissions().contains( PUBLISH_PERMISSION ) ) { 
			
			Session.NewPermissionsRequest request = new Session.NewPermissionsRequest((Activity)mContext, PUBLISH_PERMISSION);
			request.setCallback(new StatusCallback() {
				@Override
				public void call(Session session, SessionState state, Exception exception) {
					if (session != null && session.isOpened()) {
						mFecebookListener.onCallReintent();
						
						//						String msg = getString(R.string.uplodad_facebook_reintent); 
						//						Log.e(MainActivity.class.getSimpleName(), msg);
						//
						//						// Métrica
						//						mGaTracker.sendEvent("doPublish()", "PUBLISH_PERMISSION: session != null && session.isOpened()", msg, 0L);

						doPublish(bitmap, msgPhoto);
					}
					else if (exception != null) {
						
						mFecebookListener.onCallException(exception.getMessage());
						
						//						String msg = mContext.getString(R.string.error_hacer_login_fecebook) + exception.getMessage(); 
						//						Log.e(MainActivity.class.getSimpleName(), msg);
						//						
						//						// Métrica
						//						mGaTracker.sendEvent("doPublish()", "PUBLISH_PERMISSION:exception", msg, 0L);

					}
				}
			});
			
			session.requestNewPublishPermissions(request);
		}
		
		/*
		 * Hay permiso !!!
		 */
		else {
	           Request request = Request.newUploadPhotoRequest(Session.getActiveSession(), bitmap, 
	        		   new Request.Callback() {
	                @Override
	                public void onCompleted(Response response) {
	                	if (response.getGraphObject() != null) {
							mFecebookListener.onCompleted(response.getGraphObject().toString());

	                		//							String msg = getString(R.string.uplodad_facebook_ok) 
	                		//									+"\n"
	                		//									+ response.getGraphObject().toString(); 
	                		//							Log.e(MainActivity.class.getSimpleName(), msg);
	                		//							
	                		//							// Métrica
	                		//							mGaTracker.sendEvent("onCompleted()", "Request.newUploadPhotoRequest", msg, 0L);
	                		//
	                		//	                		Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();

	                		
	                	}

	                }
	            });


	        // Get the current parameters for the request
	           Bundle params = request.getParameters();
	           // Add the parameters you want, the caption in this case
	           
	           params.putString("name", msgPhoto);

	           // Update the request parameters
	           request.setParameters(params);

	           // Execute the request
	           Request.executeBatchAsync(request);

//       		JSONObject me = new JSONObject(request("me"));
//       		String id = me.getString("id");

	           
		}
	}


	
    
	private void saveFacebookSession() {
		
		Session session = new Session(mContext);
		Bundle facebookCache = new Bundle();
		        Parcel parceledCache = Parcel.obtain();
		        Session.saveSession(session, facebookCache);
		        parceledCache.writeBundle(facebookCache);
		        try {
		        	FileOutputStream output = mContext.openFileOutput("facebook",Context.MODE_PRIVATE);
//		            FileOutputStream output = openFileOutput(FACEBOOK_SESSION_FILE, MODE_PRIVATE);
		            byte[] marshalledParcel = parceledCache.marshall();

		            SharedPreferences pref = mContext.getSharedPreferences("Session Data", Context.MODE_PRIVATE);
		            Editor prefsEditor = pref.edit();
		            prefsEditor.putInt("RESTORE_BYTE_COUNT", marshalledParcel.length);
		            prefsEditor.commit();

		            output.write(marshalledParcel);
		            output.flush();
		            output.close();
		        } catch (Exception e) {
		            Log.e(getClass().getName(), "Could not save the facebook session to storage");
		        }
		}



		private boolean restoreFacebookSession() {
			Session session = new Session(mContext);
		        try {
		        	FileInputStream input = mContext.openFileInput("fecebook");

		        	SharedPreferences pref = mContext.getSharedPreferences("Session Data", Context.MODE_PRIVATE);
		            Editor prefsEditor = pref.edit();
		            int arraySize = ((SharedPreferences) prefsEditor).getInt("RESTORE_BYTE_COUNT", -1);

		            if(arraySize == -1) {
		                Log.e(getClass().getName(), "Could not read the facebook restore size");
		                return false;
		            }
		            byte[] sessionData = new byte[arraySize];
		            input.read(sessionData);
		            Parcel readParcel = Parcel.obtain();
		            readParcel.unmarshall(sessionData, 0, arraySize);
		            Bundle sessionBundle = readParcel.readBundle();
		            session = Session.restoreSession(mContext.getApplicationContext(), null, (StatusCallback)this, sessionBundle);
		            return true;
		        } catch (Exception e) {
		            Log.e(getClass().getName(), "Could not restore the session"); 
		            return false;
		        }
		    }
	
		public void savedSessionFacebook() {
			Session session = Session.getActiveSession();
			if (session != null) {
				if ( session.isOpened() ) {

					String token = session.getAccessToken();

					/*
					 *  Android Example:
					 *  When the user has logged in via SSO, save the session:
					 */
					Editor editor = mContext.getSharedPreferences("facebook-session", 
							Context.MODE_PRIVATE).edit();
					editor.putString("access_token", session.getAccessToken());
					//editor.putLong("expires_in", session.getAccessExpires());
					//editor.putLong("expires_in", session.getExpirationDate());

					/*
					 * When you app starts, in onCreate, restore the session if it exists:
					 */
					SharedPreferences savedSession = mContext.getApplicationContext().getSharedPreferences
							("facebook-session",Context.MODE_PRIVATE);
//					session.setAccessToken(savedSession.getString("access_token", null));
//					session.setAccessExpires(savedSession.getLong("expires_in", 0));

				}
			}
			else {
//				mLoginButton.setEnabled(true);
			}
		}
		
		
	
	
		public void saveDidLogin(boolean isLogin) {
			
			// Guardar en preferencias si el usuario ha hecho login.
			SharedPreferences pref = mContext.getSharedPreferences(FACEBOOK_SESSION, Context.MODE_PRIVATE);
			SharedPreferences.Editor edit = pref.edit();
			edit.putBoolean(FACEBOOK_SESSION_LOGIN, isLogin); 
			edit.commit();
			
			boolean result = isDidLogin();
		
		}

		/*
		 * El usuario salio la última vez en estado login o logout ?
		 */
		public boolean isDidLogin() {
			

					SharedPreferences pref = mContext.getApplicationContext().getSharedPreferences
							(FACEBOOK_SESSION,Context.MODE_PRIVATE);
					if (pref != null)
						return pref.getBoolean(FACEBOOK_SESSION_LOGIN, false);
					else					
						return false; 
						
		
		}
		
	public boolean isSessionOpened() {

		SharedPreferences pref = mContext.getSharedPreferences("Session Data", Context.MODE_PRIVATE);
		return pref.getBoolean("Session Facebook op", false);

	}
	
    private void shareSession(Session session) {
        Facebook facebook = new Facebook(session.getApplicationId());
        facebook.setSession(session);
        facebook.dialog(mContext, "feed", null);
//        pendingShare = false;
    }

	
	public void restoreSessionInstanceState(Bundle savedInstanceState) {
//    pendingShare = savedInstanceState.getBoolean("pendingShare");
//		Session session = Session.restoreSession(this, null, new SessionCallback(), savedInstanceState);
    Session session = Session.restoreSession(mContext, null, null, savedInstanceState); 
//		Session session = Session.restoreSession(this, null, new SessionCallback(), savedInstanceState);
    shareSession(session);
    
	}
	
	
	class SessionCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            if (state.isOpened()) {
                shareSession(session);
            }
        }
    }
	
	
	public Bundle saveSessionInstanceState(Bundle outState) {

		Session session = Session.getActiveSession();

//        outState.putBoolean("pendingShare", pendingShare);
        Session.saveSession(session, outState);
        
        return outState;
	}	
	

	public Session getActiveSession()	{
		return Session.getActiveSession();
	}


	public boolean isOpened()	{
		return Session.getActiveSession().isOpened();
	}


	
	   public void executeMeRequest(Session session) {
		   
	        Bundle bundle = new Bundle();
	        bundle.putString("fields", "picture");
	        final Request request = new Request(session, "me", bundle,
	                HttpMethod.GET, new Request.Callback() {

	            @Override
	            public void onCompleted(Response response) {
	                GraphObject graphObject = response.getGraphObject();
	                if(graphObject != null) {
	                    try {

	                    	// ProfilePicture del usuario
	                    	JSONObject jsonObject = graphObject.getInnerJSONObject();
	                        String userId = jsonObject.getString("id");
                    	    mProfilePicture.setProfileId(userId);


//	                    	JSONObject jsonObject = graphObject.getInnerJSONObject();
//	                        JSONObject obj = jsonObject.getJSONObject("picture").getJSONObject("data");
//	                        final String url = obj.getString("url");
////	                        JSONObject obj2 = jsonObject.getJSONObject("picture");
////	                        String id = jsonObject.getJSONObject("picture").getString("id");
////	                    	String id = obj.getString("id");
//	                            new Thread(new Runnable() {
//
//	                                @Override
//	                                public void run() {
//
//	                                    final Bitmap bitmap = BitmapFactory.decodeStream(UtilsNet.HttpRequest(url));
//	                                    runOnUiThread(new Runnable() {
//
//	                                        @Override
//	                                        public void run() {
////	                                            imageView.setImageBitmap(bitmap);
//	                                        	mFecebookListener.onSetProfile(bitmap);
//	                                        }
//	                                    });
//	                                }
//	                            }).start();
	                    } catch (JSONException e) {
	                        e.printStackTrace();
	                    }
	                }
	            }
	        });
	        Request.executeBatchAsync(request);
	    }

	   
	    /**
	     * Actualiza boton para publicar
	     */
	    public void updateButtonPublish (boolean enable) {
	    	
	    	/*
	    	 * Activa boton "publish".
	    	 */
//	    	Session session = Session.getActiveSession();
	    	if (FacebookFragment.this.getActiveSession() != null && FacebookFragment.this.isOpened()  && mPhoto != null && enable ) {
//	    		if (session != null && session.isOpened()  && mPhoto != null && enable ) {
	    		mPublishImageView.setImageDrawable(getResources().getDrawable(R.drawable.facebook_button_petty));
	    		mPublishImageView.setEnabled(true);
	    		mPublishTextView.setTextColor(Color.RED);
	    		//    		mPublishTextView.setVisibility(View.VISIBLE);
	    		//    		mVoidTextView.setVisibility(View.GONE);

	    		// Animacion subtexto del boton de "publish".
	    		Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.shake);
	    		mPublishImageView.startAnimation(anim);
	    		mPublishTextView.startAnimation(anim);

	    		//			Animation slideUpIn = AnimationUtils.loadAnimation(MainActivity.this, R.anim.scale);
	    		//			mPublishTextView.startAnimation(slideUpIn);

	    	}

	    	/*
	    	 * Desactiva boton "publish".
	    	 */
	    	else {
	    		mPublishImageView.setImageDrawable(getResources().getDrawable(R.drawable.facebook_button_petty_off));
	    		mPublishImageView.setEnabled(false);
	    		mPublishTextView.setTextColor(Color.GRAY);
//	    		mPublishTextView.setVisibility(View.GONE);
//	    		mVoidTextView.setVisibility(View.VISIBLE);
	    	}

	    }

	    public void setPhoto(Bitmap photo) {
	    	this.mPhoto = photo;
	    }
	   
	    public void setMsgPhoto(String msgPhoto) {
	    	this.mMsgPhoto = msgPhoto;
	    }
	    
}
