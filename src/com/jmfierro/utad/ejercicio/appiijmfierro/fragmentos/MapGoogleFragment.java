package com.jmfierro.utad.ejercicio.appiijmfierro.fragmentos;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jmfierro.utad.ejercicio.appiijmfierro.R;

import android.app.Activity;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

/**
 * This shows how UI settings can be toggled.
 */
public class MapGoogleFragment extends Fragment {
    private GoogleMap mMap;
    private UiSettings mUiSettings;

    @Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.ui_settings_demo); 
//        setUpMapIfNeeded();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {

    	View view = inflater.inflate(R.layout.map_google_fragment, container, false);
    	
        setUpMapIfNeeded();
    	
    	return view;
    }
    
    
    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
        mUiSettings = mMap.getUiSettings();
        
    }

//    LocationManager service = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
//    Criteria criteria = new Criteria();
//    String provider = service.getBestProvider(criteria, false);
//    Location location = service.getLastKnownLocation(provider);
//    LatLng userLocation = new LatLng(location.getLatitude(),location.getLongitude());
//    
////    mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
//    Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.photo4);
//	//                final int THUMBNAIL_SIZE = 64;
//	final int THUMBNAIL_SIZE = 50;
//	Bitmap photoTumbails = Bitmap.createScaledBitmap(bitmap, THUMBNAIL_SIZE, THUMBNAIL_SIZE, false);
//    mMap.addMarker(new MarkerOptions().position(
//    		new LatLng(location.getLatitude(), location.getLongitude())).title("My Location")
//    		.icon(BitmapDescriptorFactory.fromBitmap(photoTumbails)));
//    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));

    
    public LatLng getLocation (Activity activity) {
        LocationManager service = (LocationManager) activity.getSystemService(activity.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = service.getBestProvider(criteria, false);
        Location location = service.getLastKnownLocation(provider);
        
        LatLng latLng = null;
		if (location != null)
        	latLng  = new LatLng(location.getLatitude(),location.getLongitude());
        
        return latLng;
    }
    
    
    public void setImageMap (Bitmap imageBitmap, String titleText) {
        
        LatLng location = getLocation(getActivity());
        if (location != null)
        	setImageMap(imageBitmap, 95, titleText, location.latitude, location.longitude, 15);
    }

    public void setImageMap (Bitmap imageBitmap, String titleText, double latitudDouble, double longitudDouble) {
        
    	setImageMap(imageBitmap, 95, titleText, latitudDouble, longitudDouble, 15);
    }

    
    public void setImageMap (Bitmap imageBitmap, int thumbailSizeInt, String titleText, double latitudDouble, double longitudDouble, int zoomInt) {
        
    	if (imageBitmap != null) {
    		Bitmap photoTumbails = Bitmap.createScaledBitmap(imageBitmap, thumbailSizeInt, thumbailSizeInt, false);
//    		photoTumbails = UtilsMultimedia.adjustOpacity(photoTumbails, 100);
    		mMap.addMarker(new MarkerOptions().position(
    				new LatLng(latitudDouble, longitudDouble)).title(titleText)
    				.icon(BitmapDescriptorFactory.fromBitmap(photoTumbails)));
    	}
    	mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitudDouble,longitudDouble)));
//    	mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudDouble,longitudDouble), zoomInt));
//    	setLocationMap(latitudDouble, longitudDouble, zoomInt);

    }

    
    public void setLocationMap () {
    	   LatLng location = getLocation(getActivity());
    	   
    	   if (location  != null)
    		   setLocationMap(location.latitude, location.longitude, 15);
    }
    
    public void setLocationMap (double latitudDouble, double longitudDouble) {
 		   setLocationMap(latitudDouble,  longitudDouble, 15);
    }
   
    public void setLocationMap (double latitudDouble, double longitudDouble, int zoomInt) {
        	    	mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudDouble,longitudDouble), zoomInt));
    	
    }
    
    /**
     * Checks if the map is ready (which depends on whether the Google Play services APK is
     * available. This should be called prior to calling any methods on GoogleMap.
     */
    private boolean checkReady() {
        if (mMap == null) {
            Toast.makeText(getActivity(), R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void setZoomButtonsEnabled(View v) {
        if (!checkReady()) {
            return;
        }
        // Enables/disables the zoom controls (+/- buttons in the bottom right of the map).
        mUiSettings.setZoomControlsEnabled(((CheckBox) v).isChecked());
    }

    public void setCompassEnabled(View v) {
        if (!checkReady()) {
            return;
        }
        // Enables/disables the compass (icon in the top left that indicates the orientation of the
        // map).
        mUiSettings.setCompassEnabled(((CheckBox) v).isChecked());
    }

    public void setMyLocationButtonEnabled(View v) {
        if (!checkReady()) {
            return;
        }
        // Enables/disables the my location button (this DOES NOT enable/disable the my location
        // dot/chevron on the map). The my location button will never appear if the my location
        // layer is not enabled.
        mUiSettings.setMyLocationButtonEnabled(((CheckBox) v).isChecked());
    }

    public void setMyLocationLayerEnabled(View v) {
        if (!checkReady()) {
            return;
        }
        // Enables/disables the my location layer (i.e., the dot/chevron on the map). If enabled, it
        // will also cause the my location button to show (if it is enabled); if disabled, the my
        // location button will never show.
        mMap.setMyLocationEnabled(((CheckBox) v).isChecked());
    }

    public void setScrollGesturesEnabled(View v) {
        if (!checkReady()) {
            return;
        }
        // Enables/disables scroll gestures (i.e. panning the map).
        mUiSettings.setScrollGesturesEnabled(((CheckBox) v).isChecked());
    }

    public void setZoomGesturesEnabled(View v) {
        if (!checkReady()) {
            return;
        }
        // Enables/disables zoom gestures (i.e., double tap, pinch & stretch).
        mUiSettings.setZoomGesturesEnabled(((CheckBox) v).isChecked());
    }

    public void setTiltGesturesEnabled(View v) {
        if (!checkReady()) {
            return;
        }
        // Enables/disables tilt gestures.
        mUiSettings.setTiltGesturesEnabled(((CheckBox) v).isChecked());
    }

    public void setRotateGesturesEnabled(View v) {
        if (!checkReady()) {
            return;
        }
        // Enables/disables rotate gestures.
        mUiSettings.setRotateGesturesEnabled(((CheckBox) v).isChecked());
    }
}
