package com.jmfierro.utad.ejercicio.appiijmfierro.fragmentos;

import com.jmfierro.utad.ejercicio.appiijmfierro.R;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

public class CompassFragment extends Fragment implements SensorEventListener {

    private ImageView image;
    private float currentDegree = 0f;
    private SensorManager mSensorManager;

    TextView tvHeading;

    /*
     * Callback
     */
    private OnOrientationListener mOrientationListener;

	private Object mOrientation;
    
    public interface OnOrientationListener {
    	public void onSetOrientation (String orientation);
    }
    
    private static OnOrientationListener CallbackVacios = new OnOrientationListener() {
		
		@Override
		public void onSetOrientation(String orientation) {
			
		}
	};
	
     /**
     * Constructor vacio para poder instanciarlo
     */
    public CompassFragment () {
    	
    }

    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	
    	View view = inflater.inflate(R.layout.compass_fragment, container, false);
    	
      image = (ImageView) view.findViewById(R.id.compassIimageView);
      tvHeading = (TextView) view.findViewById(R.id.gradosTextVew);
      mSensorManager = (SensorManager) getActivity().getSystemService(getActivity().SENSOR_SERVICE);
    	
      
    	// TODO Auto-generated method stub
//    	return super.onCreateView(inflater, container, savedInstanceState);
      return view;
    }
    
    @Override
	public void onResume() {
        super.onResume();

        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
        
//		Matrix matrix = new Matrix();
//		matrix.postSkew(0.76f, 0.21f);
//
//		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.img_compass);
//		Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
//		image.setImageBitmap(resizedBitmap);

    }

    @Override
    public void onAttach(Activity activity) {
    	super.onAttach(activity);
    	
    	if(!(activity instanceof OnOrientationListener)) {
    		throw new IllegalStateException(getString(R.string.compass_error_on_orientationlisterner));
    	}
    	
    	mOrientationListener = (OnOrientationListener) activity;
    }
    
    
    @Override
    public void onDetach() {
    	super.onDetach();
    	
    	mOrientationListener = CallbackVacios;
    }
    
    
    @Override
	public void onPause() {
        super.onPause();
        
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        // Rotacion eje-z.
        float degree = Math.round(event.values[0]);
        String orientation = degreesToCardinalPoint((int)degree, 20);
        orientation = Integer.toString((int)degree)
        		+ "º"
        		+ " " 
        		+ orientation;
        
        mOrientationListener.onSetOrientation(orientation);

        tvHeading.setText(orientation);

        // Animación.
        RotateAnimation ra = new RotateAnimation(
                currentDegree, 
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f, 
                Animation.RELATIVE_TO_SELF,
                0.5f);

        ra.setDuration(210);
        ra.setFillAfter(true);

        image.startAnimation(ra);
        currentDegree = -degree;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
    

    public String getOrientation (int degrees) {
    	
    	return degreesToCardinalPoint(degrees, 10);
    	
    }
    
    public String degreesToCardinalPoint (int degrees, int desviation) {
    	
    	String orientation = null;

    	/*
    	 * Saca el punto cardinal.
    	 */
    	if (degrees >= (360-desviation) || degrees <= (0+desviation)) 
    		orientation = getString(R.string.north);
    	else if (degrees > desviation && degrees < (90-desviation))
    		orientation = getString(R.string.northeast);
    	else if (degrees >= (90-desviation) && degrees <= (90+desviation))
    		orientation = getString(R.string.east);
    	else if (degrees > (90+desviation) && degrees < (180-desviation))
    		orientation = getString(R.string.southeast);
    	else if (degrees >= (180-desviation) && degrees <= (180+desviation))
    		orientation = getString(R.string.south);
    	else if (degrees > (180+desviation) && degrees < (260-desviation))
    		orientation = getString(R.string.southwest);
    	else if (degrees >= (260-desviation) && degrees <= (260+desviation))
    		orientation = getString(R.string.west);
    	else if (degrees > (260+desviation) && degrees < (360-desviation))
    		orientation = getString(R.string.northwest);

//    	Float.toString(degree) + " " + getString(R.string.grados);
    		
    	return orientation;
    }
}