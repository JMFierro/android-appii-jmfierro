package com.jmfierro.utad.ejercicio.appiijmfierro.fragmentos;

import java.io.File;
import java.util.ArrayList;

import com.jmfierro.utad.ejercicio.appiijmfierro.R;
import com.jmfierro.utad.ejercicio.appiijmfierro.utils.Rotate3dAnimation;
import com.jmfierro.utad.ejercicio.appiijmfierro.utils.UtilsMultimedia;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

public class ListPhotosFragment extends Fragment implements
        AdapterView.OnItemClickListener, View.OnClickListener {
	
	public static String KEY_ADDRESS = "address";
	public static String KEY_PHOTOS = "photos";
	public static String KEY_CLICK_VIEW = "Click View";
	
    private ListView mPhotosList;
    private ViewGroup mContainer;
    private ImageView mImageView;
    private ArrayList <String> mPicturesArrayList = new ArrayList<String>();
    private ArrayList <String> mPicturesPathArrayList = new ArrayList<String>();


    
    /**
     * Callback
     */
    private OnListPhotosListener mListPhotosListener;
    public interface OnListPhotosListener {
    	public void onSetPhotoForPublish(Bitmap photoForPublis);
    	public void onClickItem();
    }
    
    private static OnListPhotosListener CallbacksVacios = new OnListPhotosListener() {
		
		@Override
		public void onSetPhotoForPublish(Bitmap photoForPublish) {
		}

		@Override
		public void onClickItem() {
			// TODO Auto-generated method stub
			
		}
	};
    		
    @Override
    public void onAttach(Activity activity) {
    	super.onAttach(activity);
    	
    	if (!(activity instanceof OnListPhotosListener)) 
    		throw new IllegalStateException(getResources().getString(R.string.listPhotos_error_on_locationlisterner)); 
    	
    	mListPhotosListener = (OnListPhotosListener) activity;
    }
    
    @Override
    public void onDetach() {
    	super.onDetach();
    	
    	mListPhotosListener = CallbacksVacios;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
//    	if (getArguments().containsKey(KEY_ADDRESS)) 
//    		mAddress = getArguments().getStringArray(KEY_ADDRESS);
    	
        /*
         * Lee ficheros del directorio picture.
         */
    	//        File file = MainActivity.getFilePhotos();
    	File file = new File(Environment.getExternalStoragePublicDirectory(
    			Environment.DIRECTORY_PICTURES), ListPhotosFragment.this.getString(R.string.app_name));

    	File[] pictures = file.listFiles();
    	//        ArrayList <String> mPicturesArrayList = new ArrayList<String>();
    	if (pictures != null)
    		if (pictures.length > 0)
    			//        		for (int i=0; i < pictures.length; i++){ 
    			for (int i=pictures.length-1; i >= 0; i--){ 
    				mPicturesArrayList.add(pictures[i].getName());
    				mPicturesPathArrayList.add(pictures[i].getPath());
    			}


    	// Ejemplos
    	mPicturesPathArrayList.add("*");
    	mPicturesPathArrayList.add("*");
    	mPicturesPathArrayList.add("*");
    	mPicturesArrayList.add("ejemplo1");
    	mPicturesArrayList.add("ejemplo2");
    	mPicturesArrayList.add("ejemplo3");

    }
    
    
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {

    	View view = inflater.inflate(R.layout.list_photos_fragment, container, false);
    	
        mPhotosList = (ListView) view.findViewById(android.R.id.list);
        mImageView = (ImageView) view.findViewById(R.id.picture);
        mContainer = (ViewGroup) view.findViewById(R.id.container);

        // Prepare the ListView
//        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//        		android.R.layout.simple_list_item_1, PHOTOS_NAMES);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, mPicturesArrayList);

        mPhotosList.setAdapter(adapter);
        mPhotosList.setOnItemClickListener(this);

        // Prepare the ImageView
        mImageView.setClickable(true);
        mImageView.setFocusable(true);
        mImageView.setOnClickListener(this);

        // Since we are caching large views, we want to keep their cache
        // between each animation
        mContainer.setPersistentDrawingCache(ViewGroup.PERSISTENT_ANIMATION_CACHE);
        
		return view;
    }

    /**
     * Setup a new 3D rotation on the container view.
     *
     * @param position the item that was clicked to show a picture, or -1 to show the list
     * @param start the start angle at which the rotation must begin
     * @param end the end angle of the rotation
     */
    private void applyRotation(int position, float start, float end) {
        // Find the center of the container
        final float centerX = mContainer.getWidth() / 2.0f;
        final float centerY = mContainer.getHeight() / 2.0f;

        // Animación para visulizar imagen.
        final Rotate3dAnimation rotation =
                new Rotate3dAnimation(start, end, centerX, centerY, 310.0f, true);
        rotation.setDuration(500);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new DisplayNextView(position));

        mContainer.startAnimation(rotation);
    }

    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        // Pre-load the image then start the animation
//    	mImageView.setImageResource(PHOTOS_RESOURCES[position]);
    	
    	
        // Abre file
//		File imgFile = new  File(mPicturesPathArrayList.get(position));
//		if(imgFile.exists()){
//		    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//		    //Drawable d = new BitmapDrawable(getResources(), myBitmap);
////		    ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
//			mImageView.setImageBitmap(myBitmap);
//
//		}

    	mListPhotosListener.onClickItem();
    	
    	String filePath = this.
		mPicturesPathArrayList.get(position); 
    	Bitmap photo = null;
    	if (filePath.equals("*")) {
    		int random = (int)(Math.random()*(6-1))+1;
    		switch (random) {
			case 1:
				photo = BitmapFactory.decodeResource(getResources(),R.drawable.photo1);
				break;

			case 2:
				photo = BitmapFactory.decodeResource(getResources(),R.drawable.photo2);
				break;

			case 3:
				photo = BitmapFactory.decodeResource(getResources(),R.drawable.photo3);
				break;

			case 4:
				photo = BitmapFactory.decodeResource(getResources(),R.drawable.photo4);
				break;

			case 5:
				photo = BitmapFactory.decodeResource(getResources(),R.drawable.photo5);
				break;
				
			case 6:
				photo = BitmapFactory.decodeResource(getResources(),R.drawable.photo6);
				break;


			default:
				break;
			}

            mImageView.setImageBitmap(photo);

    	}
    	else {
    		
//    		File root = Environment.getExternalStorageDirectory();
//    		ImageView IV = (ImageView) findViewById(R.id."image view");
//    		Bitmap bMap = BitmapFactory.decodeFile(root+"/images/01.jpg");
//    		IV.setImageBitmap(bMap);

    		photo = BitmapFactory.decodeFile(filePath);
    		
    		// Foto para publicar (!antes de invertirla!)
    		mListPhotosListener.onSetPhotoForPublish(photo);
    		
    		// Invertirla antes de la rotación 
    		photo = UtilsMultimedia.getBitmapMirror(photo);
            mImageView.setImageBitmap(photo);
    	}

//        mImageView.setImageBitmap(BitmapFactory.decodeStream(in));
//        mImageView.setImageResource(PHOTOS_RESOURCES[position]);
        applyRotation(position, 0, 90);
        if (photo != null)
        	mListPhotosListener.onSetPhotoForPublish(photo);
    }

    public void onClick(View v) {
    	mListPhotosListener.onSetPhotoForPublish(null);
        applyRotation(-1, 180, 90);
    }

    /**
     * Listener para el touch 
     */
    private final class DisplayNextView implements Animation.AnimationListener {
        private final int mPosition;

        private DisplayNextView(int position) {
            mPosition = position;
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            mContainer.post(new SwapViews(mPosition));
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }

    /**
     * This class is responsible for swapping the views and start the second
     * half of the animation.
     */
    private final class SwapViews implements Runnable {
        private final int mPosition;

        public SwapViews(int position) {
            mPosition = position;
        }

        public void run() {
            final float centerX = mContainer.getWidth() / 2.0f;
            final float centerY = mContainer.getHeight() / 2.0f;
            Rotate3dAnimation rotation;
            
            if (mPosition > -1) {
                mPhotosList.setVisibility(View.GONE);
                mImageView.setVisibility(View.VISIBLE);
                mImageView.requestFocus();

                rotation = new Rotate3dAnimation(90, 180, centerX, centerY, 310.0f, false);
            } else {
                mImageView.setVisibility(View.GONE);
                mPhotosList.setVisibility(View.VISIBLE);
                mPhotosList.requestFocus();

                rotation = new Rotate3dAnimation(90, 0, centerX, centerY, 310.0f, false);
            }

            rotation.setDuration(500);
            rotation.setFillAfter(true);
            rotation.setInterpolator(new DecelerateInterpolator());

            mContainer.startAnimation(rotation);
        }
    }

}
