package com.jmfierro.utad.ejercicio.appiijmfierro.fragmentos;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.jmfierro.utad.ejercicio.appiijmfierro.R;
import com.jmfierro.utad.ejercicio.appiijmfierro.R.id;
import com.jmfierro.utad.ejercicio.appiijmfierro.R.layout;
import com.jmfierro.utad.ejercicio.appiijmfierro.R.string;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
//import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class LocationFragment extends Fragment {
	
	private double mLatitud;
	private double mLongitud;
	private String mAddress; 

	// Localizacion
	private LocationManager mLocationManager;
//    private static final int UPDATE_TIME = 60000;
    private static final int UPDATE_METERS = 10;
    private static final int UPDATE_TIME = 1000 * 60 * 5;

	private LocationFragment mLocation;

	private boolean mGeocoderAvailable;

	private TextView mAddressTextView;

	/**
	 * Callback
	 */
	private OnLocationListener mLocationListener; 

	public interface OnLocationListener {
//		public void onSetLocation(double aLatitud, double aLongitud);
		public void onSetLocation(String address);
	}

	
	private static OnLocationListener CallbacksVacios = new OnLocationListener() {
		
		@Override
		public void onSetLocation(String address) {
		}
	}; 

		
	
	/*
     * Constructor vacio para instanciarlo
     */
    public LocationFragment () {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
        // The isPresent() helper method is only available on Gingerbread or above.
        mGeocoderAvailable =
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && Geocoder.isPresent();

    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.location_fragment, container, false);

        mAddressTextView = (TextView) view.findViewById(R.id.address);
    	
		return view;
    }

    
    @Override
    public void onAttach(Activity activity) {
    	super.onAttach(activity);
    	
    	if (!(activity instanceof OnLocationListener)) {
    		throw new IllegalStateException(getString(R.string.localitation_error_on_locationlisterner));
    	}
    	
    	mLocationListener = (OnLocationListener) activity;
    }

    
    @Override
    public void onDetach() {
    	super.onDetach();
    	
    	mLocationListener = CallbacksVacios;
    }

    
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//    	super.onActivityCreated(savedInstanceState);
//    	useFineProvider(getView());
////        mUseFine = savedInstanceState.getBoolean(KEY_FINE);
//    }
    
//    // Restores UI states after rotation.
//    @Override
//	public void onSaveInstanceState(Bundle outState) {  
//        super.onSaveInstanceState(outState);
//        outState.putBoolean(KEY_FINE, mUseFine);
//        outState.putBoolean(KEY_BOTH, mUseBoth);
//    }

//    @Override
//	public void onResume() {
//        super.onResume();
//        setup();
//    }

    @Override
	public void onStart() {
        super.onStart();

        // Check if the GPS setting is currently enabled on the device.
        // This verification should be done during onStart() because the system calls this method
        // when the user returns to the activity, which ensures the desired location provider is
        // enabled each time the activity resumes from the stopped state.
        // Get a reference to the LocationManager object.
        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        // Localizacion
		setup();

        
//        LocationManager locationManager =
//                (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
//        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//
//        if (!gpsEnabled) {
//            // Build an alert dialog here that requests that the user enable
//            // the location services, then when the user clicks the "OK" button,
//            // call enableLocationSettings()
//            new EnableGpsDialogFragment().show(getActivity().getSupportFragmentManager(), "enableGpsDialog");
//        }
    }
//
//    // Method to launch Settings
//    private void enableLocationSettings() {
//        Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//        startActivity(settingsIntent);
//    }

    // Stop receiving location updates whenever the Activity becomes invisible.
    @Override
	public void onStop() {
        super.onStop();
        mLocationManager.removeUpdates(listener);
    }

    
	private final LocationListener listener = new LocationListener () {

		@Override
		public void onLocationChanged(Location location) {
			updateLocation(location);	
			Toast.makeText(getActivity(), getString(R.string.localitation_update)
					+ "\n!"
					+ getString(R.string.localitation_update_interval_minutes), Toast.LENGTH_LONG).show();
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}
		
	};
	
    // Set up fine and/or coarse location providers depending on whether the fine provider or
    // both providers button is pressed.
    private void setup() {
        Location gpsLocation = null;
        Location networkLocation = null;
        mLocationManager.removeUpdates(listener);
        mAddressTextView.setText(R.string.unknown);
        // Get fine location updates only.
//        if (mUseFine) {
            // Request updates from just the fine (gps) provider.
//            gpsLocation = requestUpdatesFromProvider(
//                    LocationManager.GPS_PROVIDER, R.string.not_support_gps);
//            // Update the UI immediately if a location is obtained.
//            if (gpsLocation != null) updateUILocation(gpsLocation);
//        }
//        else if (mUseBoth) {
            // Get coarse and fine location updates.
            // Request updates from both fine (gps) and coarse (network) providers.
            gpsLocation = requestUpdatesFromProvider(
                    LocationManager.GPS_PROVIDER, R.string.not_support_gps);
            networkLocation = requestUpdatesFromProvider(
                    LocationManager.NETWORK_PROVIDER, R.string.not_support_network);

            // If both providers return last known locations, compare the two and use the better
            // one to update the UI.  If only one provider returns a location, use it.
            if (gpsLocation != null && networkLocation != null) {
                updateLocation(getBetterLocation(gpsLocation, networkLocation));
            } else if (gpsLocation != null) {
                updateLocation(gpsLocation);
            } else if (networkLocation != null) {
                updateLocation(networkLocation);
            }
        }
    

private void updateLocation(Location location) {
    // We're sending the update to a handler which then updates the UI with the new
    // location.
//    Message.obtain(mHandler,
//            UPDATE_LATLNG,
//            location.getLatitude() + ", " + location.getLongitude()).sendToTarget();
	mLatitud = location.getLatitude();
	mLongitud = location.getLongitude();
	

    // Bypass reverse-geocoding only if the Geocoder service is available on the device.
    if (mGeocoderAvailable) doReverseGeocoding(location);
}


private void doReverseGeocoding(Location location) {
    // Since the geocoding API is synchronous and may take a while.  You don't want to lock
    // up the UI thread.  Invoking reverse geocoding in an AsyncTask.
	new ReverseGeocodingTask(getActivity()).execute(new Location[] {location});
//    new ReverseGeocodingTask(this).execute(new Location[] {location});
//    (new ReverseGeocodingTask(this)).execute(new Location[] {location});
}


/**
 * Method to register location updates with a desired location provider.  If the requested
 * provider is not available on the device, the app displays a Toast with a message referenced
 * by a resource id.
 *
 * @param provider Name of the requested provider.
 * @param errorResId Resource id for the string message to be displayed if the provider does
 *                   not exist on the device.
 * @return A previously returned {@link android.location.Location} from the requested provider,
 *         if exists.
 */
private Location requestUpdatesFromProvider(final String provider, final int errorResId) {
    Location location = null;
    if (mLocationManager.isProviderEnabled(provider)) {
        mLocationManager.requestLocationUpdates(provider, UPDATE_TIME, UPDATE_METERS, listener);
        location = mLocationManager.getLastKnownLocation(provider);
    } else {
        Toast.makeText(getActivity(), errorResId, Toast.LENGTH_LONG).show();
    }
    return location;
}


/** Determines whether one Location reading is better than the current Location fix.
 * Code taken from
 * http://developer.android.com/guide/topics/location/obtaining-user-location.html
 *
 * @param newLocation  The new Location that you want to evaluate
 * @param currentBestLocation  The current Location fix, to which you want to compare the new
 *        one
 * @return The better Location object based on recency and accuracy.
 */
protected Location getBetterLocation(Location newLocation, Location currentBestLocation) {
   if (currentBestLocation == null) {
       // A new location is always better than no location
       return newLocation;
   }

   // Check whether the new location fix is newer or older
   long timeDelta = newLocation.getTime() - currentBestLocation.getTime();
   boolean isSignificantlyNewer = timeDelta > UPDATE_TIME;
   boolean isSignificantlyOlder = timeDelta < -UPDATE_TIME;
   boolean isNewer = timeDelta > 0;

   // If it's been more than two minutes since the current location, use the new location
   // because the user has likely moved.
   if (isSignificantlyNewer) {
       return newLocation;
   // If the new location is more than two minutes older, it must be worse
   } else if (isSignificantlyOlder) {
       return currentBestLocation;
   }

   // Check whether the new location fix is more or less accurate
   int accuracyDelta = (int) (newLocation.getAccuracy() - currentBestLocation.getAccuracy());
   boolean isLessAccurate = accuracyDelta > 0;
   boolean isMoreAccurate = accuracyDelta < 0;
   boolean isSignificantlyLessAccurate = accuracyDelta > 200;

   // Check if the old and new location are from the same provider
   boolean isFromSameProvider = isSameProvider(newLocation.getProvider(),
           currentBestLocation.getProvider());

   // Determine location quality using a combination of timeliness and accuracy
   if (isMoreAccurate) {
       return newLocation;
   } else if (isNewer && !isLessAccurate) {
       return newLocation;
   } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
       return newLocation;
   }
   return currentBestLocation;
}


/** Checks whether two providers are the same */
private boolean isSameProvider(String provider1, String provider2) {
    if (provider1 == null) {
      return provider2 == null;
    }
    return provider1.equals(provider2);
}




// AsyncTask encapsulating the reverse-geocoding API.  Since the geocoder API is blocked,
// we do not want to invoke it from the UI thread.
private class ReverseGeocodingTask extends AsyncTask<Location, Void, String> {
    Context mContext;

    public ReverseGeocodingTask(Context context) {
        super();
        mContext = context;
    }

    @Override
    protected String doInBackground(Location... params) {

        Location loc = params[0];

    	String addressText = null;
    	
    	Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
            // Update address field with the exception.
//            Message.obtain(mHandler, UPDATE_ADDRESS, e.toString()).sendToTarget();
//            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
        }
        if (addresses != null && addresses.size() > 0) {
            Address address = addresses.get(0);
            // Format the first line of address (if available), city, and country name.
            addressText = String.format("%s, %s, %s",
                    address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                    address.getLocality(),
                    address.getCountryName());
            // Update address field on UI.
//            Message.obtain(mHandler, UPDATE_ADDRESS, addressText).sendToTarget();
        }
        return addressText;
    }
    
    @Override
    protected void onPostExecute(String resultAddress) {
    	
    	String address = resultAddress;
    	
    	// Actualiza UI
    	mAddressTextView.setText(address);
    	
    	// Callback para actualizar dirección 
    	mLocationListener.onSetLocation(address);

    	
    }
  }

 public String getAddress () {
	 return mAddress;
 }
 
 public double getLatitud () {
	 return mLatitud;
 }
 public double getLongitud () {
	 return mLongitud;
 }
 
}
