package com.jmfierro.utad.ejercicio.appiijmfierro.utils;

import java.lang.reflect.Method;
import java.util.List;


import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Build;
import android.view.Surface;

public class UtilsCamera {


	
	static public Bitmap getBitmapMirror (Bitmap bitmap) {

		Matrix matrix = new Matrix(); 
		matrix.preScale(-1, 1);

		//  matrix.invert();
		return Bitmap.createBitmap(bitmap, 0, 0, 
				bitmap.getWidth(), bitmap.getHeight(), 
				matrix, true);
	}

	static public Bitmap getBtimapRotate (Bitmap bitmap, int orientation) {
		Matrix matrix = new Matrix(); 

		//  // Perform matrix rotations/mirrors depending on camera that took the photo
		//  Camera.CameraInfo info = new Camera.CameraInfo();
		//  if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
		//  {
		//      float[] mirrorY = { -1, 0, 0, 0, 1, 0, 0, 0, 1};
		//      Matrix matrixMirrorY = new Matrix();
		//      matrixMirrorY.setValues(mirrorY);
		//
		//      matrix.postConcat(matrixMirrorY);
		//  }

		matrix.postRotate(orientation);
		return Bitmap.createBitmap(bitmap, 0, 0, 
				bitmap.getWidth(), bitmap.getHeight(), 
				matrix, true);


	}
	

//    static public int get2OrientationAlrightOfPhothoCamera(Activity activity, int cameraId, android.hardware.Camera camera) {
//
//    	int orientation = activity.getResources().getConfiguration().orientation;
//
//        if (orientation == Configuration.ORIENTATION_UNDEFINED) return 0;  //RIENTATION_UNKNOWN) return;
//        android.hardware.Camera.CameraInfo info =
//               new android.hardware.Camera.CameraInfo();
//        camera.getCameraInfo(cameraId, info);
//        orientation = (orientation + 45) / 90 * 90;
//        int rotation = 0;
//        if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
//            rotation = (info.orientation - orientation + 360) % 360;
//            rotation = rotation - 180;
//        } else {  // back-facing camera
//            rotation = (info.orientation + orientation) % 360;
//        }
//        return rotation;
//    }
    
//    static public void setDisplayOrientationCamera(Camera camera, int angle){
//        Method downPolymorphic;
//        try
//        {
//            downPolymorphic = camera.getClass().getMethod("setDisplayOrientation", new Class[] { int.class });
//            if (downPolymorphic != null)
//                downPolymorphic.invoke(camera, new Object[] { angle });
//        }
//        catch (Exception e1)
//        {
//        }
//    }


	
	/**
	 * Orientación para el preview.
	 * 
	 * Tiene en cuenta cuatro posiciones:
	 *    Portrat (derecho y la reves)
	 *    Landscape (derecho y la reves)
	 */
    static public void setOrientationCameraPreview(Activity activity,
            int cameraId, android.hardware.Camera camera) {
//        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
//        android.hardware.Camera.getCameraInfo(cameraId, info);
//        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
//        int degrees = 0;
//        switch (rotation) {
//            case Surface.ROTATION_0: degrees = 0; break;
//            case Surface.ROTATION_90: degrees = 90; break;
//            case Surface.ROTATION_180: degrees = 180; break;
//            case Surface.ROTATION_270: degrees = 270; break;
//        }
//
//        int result;
//        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//        	result = (info.orientation + degrees) % 360;
//        	result = (360 - result) % 360;  // compensate the mirror
//        } else {  // back-facing
//        	result = (info.orientation - degrees + 360) % 360;
//        }

        
        //        camera.setDisplayOrientation(result);

    	int result = getOrientationAlrightOfPhothoCamera(activity, cameraId, camera);
    	
    	if (Integer.parseInt(Build.VERSION.SDK) >= 8)             {

        	//            UtilsCamera.setDisplayOrientationCamera(camera, result);
        	Method downPolymorphic;
        	try
        	{
        		downPolymorphic = camera.getClass().getMethod("setDisplayOrientation", new Class[] { int.class });
        		if (downPolymorphic != null)
        			downPolymorphic.invoke(camera, new Object[] { result });
        	}
        	catch (Exception e1)
        	{
        	}
        }

        else
        {
        	Parameters p = camera.getParameters();
            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            {
                p.set("orientation", "portrait");
                p.set("rotation", result);
            }
            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            {
                p.set("orientation", "landscape");
                p.set("rotation", result);
            }
        } 
    }
    
    static public int getOrientationAlrightOfPhothoCamera(Activity activity,
            int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
//            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        return result;
        
    }


//    static public void unirBitmap() {
//    Bitmap drawingBitmap = Bitmap.createBitmap(bmp1.getWidth(),bmp1.getHeight (), bmp1.getConfig());
//    canvas = new Canvas(drawingBitmap);
//    paint = new Paint();
//    canvas.drawBitmap(bmp1, 0, 0, paint);
//    paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mod e.MULTIPLY));
//    canvas.drawBitmap(bmp2, 0, 0, paint);
//    compositeImageView.setImageBitmap(drawingBitmap);
//    }
//    

    static public void setOptimalSizeCameraPreview(Camera camera, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio=(double)h / w;
        List<Camera.Size> sizes = null; 
        
        if (sizes == null) return;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }

        if(camera != null){
            //Setting the camera's aspect ratio
            Camera.Parameters parameters = camera.getParameters();
//            List<Size> sizes = parameters.getSupportedPreviewSizes();
//            Size optimalSize = getOptimalPreviewSize(sizes, activity.getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);

            parameters.setPreviewSize(optimalSize.width, optimalSize.height);
            camera.setParameters(parameters);
        }

        //        return optimalSize;
        return;
    }

  
    

}
