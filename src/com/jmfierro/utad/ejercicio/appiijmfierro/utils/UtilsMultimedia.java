package com.jmfierro.utad.ejercicio.appiijmfierro.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.jmfierro.utad.ejercicio.appiijmfierro.MainActivity;
import com.jmfierro.utad.ejercicio.appiijmfierro.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.Surface;
import android.widget.ImageView;

public class UtilsMultimedia {

	public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

	
	static public Bitmap getBitmapMirror (Bitmap bitmap) {

		Matrix matrix = new Matrix(); 
		matrix.preScale(-1, 1);

		//  matrix.invert();
		return Bitmap.createBitmap(bitmap, 0, 0, 
				bitmap.getWidth(), bitmap.getHeight(), 
				matrix, true);
	}

	static public Bitmap getBtimapRotate (Bitmap bitmap, int orientation) {
		Matrix matrix = new Matrix(); 

		//  // Perform matrix rotations/mirrors depending on camera that took the photo
		//  Camera.CameraInfo info = new Camera.CameraInfo();
		//  if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
		//  {
		//      float[] mirrorY = { -1, 0, 0, 0, 1, 0, 0, 0, 1};
		//      Matrix matrixMirrorY = new Matrix();
		//      matrixMirrorY.setValues(mirrorY);
		//
		//      matrix.postConcat(matrixMirrorY);
		//  }

		matrix.postRotate(orientation);
		return Bitmap.createBitmap(bitmap, 0, 0, 
				bitmap.getWidth(), bitmap.getHeight(), 
				matrix, true);


	}
	

//    static public int getOrientationCameraPreview(Activity activity, int cameraId, android.hardware.Camera camera) {
//
//    	int orientation = activity.getResources().getConfiguration().orientation;
//
//        if (orientation == Configuration.ORIENTATION_UNDEFINED) return 0;  //RIENTATION_UNKNOWN) return;
//        android.hardware.Camera.CameraInfo info =
//               new android.hardware.Camera.CameraInfo();
//        camera.getCameraInfo(cameraId, info);
//        orientation = (orientation + 45) / 90 * 90;
//        int rotation = 0;
//        if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
//            rotation = (info.orientation - orientation + 360) % 360;
//        } else {  // back-facing camera
//            rotation = (info.orientation + orientation) % 360;
//        }
//        return rotation;
//    }
    
    static public void setDisplayOrientationCamera(Camera camera, int angle){
        Method downPolymorphic;
        try
        {
            downPolymorphic = camera.getClass().getMethod("setDisplayOrientation", new Class[] { int.class });
            if (downPolymorphic != null)
                downPolymorphic.invoke(camera, new Object[] { angle });
        }
        catch (Exception e1)
        {
        }
    }


//    static public void setCameraDisplayOrientation(Activity activity,
//            int cameraId, android.hardware.Camera camera) {
//        android.hardware.Camera.CameraInfo info =
//                new android.hardware.Camera.CameraInfo();
//        android.hardware.Camera.getCameraInfo(cameraId, info);
//        int rotation = activity.getWindowManager().getDefaultDisplay()
//                .getRotation();
//        int degrees = 0;
//        switch (rotation) {
//            case Surface.ROTATION_0: degrees = 0; break;
//            case Surface.ROTATION_90: degrees = 90; break;
//            case Surface.ROTATION_180: degrees = 180; break;
//            case Surface.ROTATION_270: degrees = 270; break;
//        }
//
//        int result;
//        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//            result = (info.orientation + degrees) % 360;
//            result = (360 - result) % 360;  // compensate the mirror
//        } else {  // back-facing
//            result = (info.orientation - degrees + 360) % 360;
//        }
////        camera.setDisplayOrientation(result);
//        
//        if (Integer.parseInt(Build.VERSION.SDK) >= 8)
//            UtilsMultimedia.setDisplayOrientationCamera(camera, result);
//        else
//        {
//        	Parameters p = camera.getParameters();
//            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
//            {
//                p.set("orientation", "portrait");
//                p.set("rotation", result);
//            }
//            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
//            {
//                p.set("orientation", "landscape");
//                p.set("rotation", result);
//            }
//        } 
//    }
//    
//    static public int getCameraDisplayOrientation(Activity activity,
//            int cameraId, android.hardware.Camera camera) {
//        android.hardware.Camera.CameraInfo info =
//                new android.hardware.Camera.CameraInfo();
//        android.hardware.Camera.getCameraInfo(cameraId, info);
//        int rotation = activity.getWindowManager().getDefaultDisplay()
//                .getRotation();
//        int degrees = 0;
//        switch (rotation) {
//            case Surface.ROTATION_0: degrees = 0; break;
//            case Surface.ROTATION_90: degrees = 90; break;
//            case Surface.ROTATION_180: degrees = 180; break;
//            case Surface.ROTATION_270: degrees = 270; break;
//        }
//
//        int result;
//        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//            result = (info.orientation + degrees) % 360;
//            result = (360 - result) % 360;  // compensate the mirror
//        } else {  // back-facing
//            result = (info.orientation - degrees + 360) % 360;
//        }
//
//        return result;
//        
//    }


//    static public void unirBitmap() {
//    Bitmap drawingBitmap = Bitmap.createBitmap(bmp1.getWidth(),bmp1.getHeight (), bmp1.getConfig());
//    canvas = new Canvas(drawingBitmap);
//    paint = new Paint();
//    canvas.drawBitmap(bmp1, 0, 0, paint);
//    paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mod e.MULTIPLY));
//    canvas.drawBitmap(bmp2, 0, 0, paint);
//    compositeImageView.setImageBitmap(drawingBitmap);
//    }
//    

    
    
    
    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(Context context, int type){
          return Uri.fromFile(getOutputMediaFile(context,type));
    }
    
    /** Create a File for saving an image or video */
    public static File getOutputMediaFile(Context context, int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

    	File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), context.getString(R.string.app_name));    	
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d(UtilsCamera.class.getSimpleName(), "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
            "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
            "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

	

    public static Bitmap readFile(String pathString) {
        File root = Environment.getExternalStorageDirectory();
        return BitmapFactory.decodeFile(pathString);
    }

    
    private Bitmap joinImages(File first, File second)
    {
        Bitmap bmp1, bmp2;
        bmp1 = BitmapFactory.decodeFile(first.getPath());
        bmp2 = BitmapFactory.decodeFile(second.getPath());
        if (bmp1 == null || bmp2 == null)
            return bmp1;
        int height = bmp1.getHeight();
        if (height < bmp2.getHeight())
            height = bmp2.getHeight();

        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth() + bmp2.getWidth(), height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, 0, 0, null);
        canvas.drawBitmap(bmp2, bmp1.getWidth(), 0, null);
        return bmOverlay;
    }
    

    private Bitmap join2Images(File first, File second)
    {

    	// Get your images from their files
//    	Bitmap bottomImage = new BitmapFactory.decodeFile("myFirstPNG.png");
//    	Bitmap topImage = new BitmapFactory.decodeFile("myOtherPNG.png");

    	// As described by Steve Pomeroy in a previous comment, 
    	// use the canvas to combine them.
    	// Start with the first in the constructor..
    	Bitmap bottomImage = null;
		Canvas comboImage = new Canvas(bottomImage);
    	// Then draw the second on top of that
    	Bitmap topImage = null;
		comboImage.drawBitmap(topImage, 0f, 0f, null);

    	// bottomImage is now a composite of the two. 

    	// To write the file out to the SDCard:
    	OutputStream os = null;
    	try {
    		os = new FileOutputStream("/sdcard/DCIM/Camera/" + "myNewFileName.png");
    		Bitmap image = null;
			image.compress(CompressFormat.PNG, 50, os);
    	} catch(IOException e) {
    		e.printStackTrace();
    	}
		return topImage;
    }
    
    
    	public static void saveFile (Context context, Bitmap bitmap) {
    	File mediaStorageDir = new File(
    			Environment.getExternalStoragePublicDirectory(
    					Environment.DIRECTORY_PICTURES
    					),
    					context.getString(R.string.app_name)
    			);


    	if (!mediaStorageDir.exists()) {
    		if (!mediaStorageDir.mkdirs()) {
    			//                showSavingPictureErrorToast();
    			return;
    		}
    	}


    	if (!mediaStorageDir.exists()) {
    		if (!mediaStorageDir.mkdirs()) {
    			//                showSavingPictureErrorToast();
    			return;
    		}
    	}

    	
    	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    	File mediaFile = new File(
    			mediaStorageDir.getPath() + File.separator + timeStamp + ".jpg"
    			);

    	try {
    		FileOutputStream stream = new FileOutputStream(mediaFile);
    		bitmap.compress(CompressFormat.JPEG, 90, stream);  //PICTURE_QUALITY, stream);
    	} catch (IOException exception) {
    		//            showSavingPictureErrorToast();

    		//            Log.w(UtilsMultimedia.class.getSimpleName(), "IOException during saving bitmap", exception);
    		return;
    	}

    	//        MediaScannerConnection.scanFile(
    	//            context,
    	//            new String[] { mediaFile.toString() },
    	//            new String[] { "image/jpeg" },
    	//            null
    	//        );

    }

    	public static Bitmap adjustOpacity(Bitmap bitmap, int opacity) {
    		
    	    Bitmap mutableBitmap = bitmap.isMutable() ? bitmap : bitmap.copy(Bitmap.Config.ARGB_8888, true);
    	    Canvas canvas = new Canvas(mutableBitmap);
    	    int colour = (opacity & 0xFF) << 24;
    	    canvas.drawColor(colour, PorterDuff.Mode.DST_IN);
    	    return mutableBitmap;
    	}
 
    	
    	public static Bitmap imageToCircular(Bitmap bitmap) {
        	
    	Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

    	BitmapShader shader = new BitmapShader (bitmap,  TileMode.CLAMP, TileMode.CLAMP);
    	Paint paint = new Paint();
    	        paint.setShader(shader);

    	Canvas c = new Canvas(circleBitmap);
    	c.drawCircle(bitmap.getWidth()/2, bitmap.getHeight()/2, bitmap.getWidth()/2, paint);

    	return circleBitmap;
}
    	
    	
    	public  static Bitmap getRoundedCornerImage(Bitmap bitmap) {
//            Bitmap bitmap = ((BitmapDrawable)bitmapDrawable).getBitmap();
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);
            final float roundPx = 10;

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
//            Drawable image = new BitmapDrawable(output);
//            return image;
            return bitmap;

        }
    	
    	public static Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
            int targetWidth = 125;
            int targetHeight = 125;

            Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                    targetHeight, Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(targetBitmap);
            Path path = new Path();
            path.addCircle(
                    ((float) targetWidth - 1) / 2,
                    ((float) targetHeight - 1) / 2,
                    (Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
                    Path.Direction.CCW);

            canvas.clipPath(path);
            Bitmap sourceBitmap = scaleBitmapImage;
            final Paint p = new Paint();
            canvas.drawBitmap(
                    sourceBitmap,
                    new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap
                            .getHeight()), new Rect(0, 0, targetWidth,
                            targetHeight), p);
            return targetBitmap;
        }
}
